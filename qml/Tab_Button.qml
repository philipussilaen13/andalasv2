import QtQuick 2.0


Rectangle{
    property var background_color:  "white"
    width: 120
    height: 50
    color: background_color
    radius: 25
    Text{
        anchors.fill: parent;
        text: "Tab Name"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

}
