import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle
    width: 1024
    height: 768
    color: 'white'
    property alias backroundImg: background_img.source
    property var topPanelColor: 'BLUE' // 'BLUE/GREEN'
    property bool mainMode: true
    property bool watermark: false

    Image{
        id: background_img
        anchors.fill: parent
    }

    Rectangle{
        id: top_panel
        width: parent.width
        height: 90
        color: (topPanelColor=='BLUE') ? "#009BE1" : "#65B82E"
    }

    Image{
        id: top_left_logo
        source: "img/apservice/header_logo.png"
        width: parent.width/2 - 90
        height: 92
        fillMode: Image.Stretch
        visible: mainMode
    }

    Image{
        id: popbox_logo
        source: "img/apservice/logo/popbox.png"
        width: 100
        height: top_panel.height
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        visible:  mainMode
    }

    Image{
        id: other_logo
        source:  (topPanelColor=='GREEN') ? "img/apservice/logo/angkasapura_biru.png" : "img/apservice/logo/angkasapura_hijau.png";
        width: 300
        height: top_panel.height
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        visible:  !mainMode
    }

    Image{
        id: top_right_logo
        source: "img/apservice/logo/popbox.png"
        width: 100
        height: top_panel.height
        fillMode: Image.PreserveAspectFit
        anchors.right: parent.right
        anchors.rightMargin: 20
        visible: !mainMode
    }

    Image{
        id: watermark_image
        visible: watermark
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.right: parent.right
        anchors.rightMargin: 0
        source: 'img/apservice/img/bg.png'

    }



}
