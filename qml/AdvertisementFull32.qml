import QtQuick 2.4
import QtQuick.Controls 1.2
import Qt.labs.folderlistmodel 1.0
import QtMultimedia 5.0

Rectangle{
    id: parent_root
    color: "black"    
    property var img_path: "/advertisement/video/"
    property url img_path_: ".." + img_path
    property var qml_pic
    property string pic_source: ""
    property int num_pic
    property string mode // ["staticVideo", "mediaPlayer", "liveView"]
//    property var list_pic: img_files
    property variant media_files: []
    property int index: 0    
    property var set_height
    height: (set_height=="normal") ? 1152 : 1920

    property var locker_name: "PopBox Locker Name"
    property int loading_time: 5


    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log('ads mode : ' +  mode)
            if(mode=="mediaPlayer" && media_files.length == 0){
                slot_handler.start_get_file_dir(img_path);
            }
            slot_handler.start_get_locker_name()
        }
        if(Stack.status==Stack.Deactivating){
            player.stop()
            while (media_files.length > 0) {
                media_files.pop();
            }
        }
    }

    Component.onCompleted: {
        root.start_get_file_dir_result.connect(get_result)
        root.start_get_detection_result.connect(get_detection)
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.start_get_file_dir_result.disconnect(get_result)
        root.start_get_detection_result.disconnect(get_detection)
        root.start_get_locker_name_result.disconnect(show_locker_name)
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text
    }

    function get_detection(result){
        console.log("get_detection : ", JSON.stringify(result))
        if (result=="FAILED" || result=="ERROR"){
            return
        } else {
            var handle = JSON.parse(result)
            var detections = handle.result_face + handle.result_motion
            if (detections > 0){
/*                player.stop()
//                while (media_files.length > 0) {
//                    media_files.pop();
//                }
//                my_stack_view.pop()
*/                root.reference = "people_count"
                set_height = "normal"
                root.my_stack_view_visible = true
                root.reference = "people_count"
                media_mode.anchors.horizontalCenter = parent_root.horizontalCenter
                media_mode.anchors.verticalCenter = parent_root.verticalCenter
                welcome_window.visible = true
                show_timer_loading.start()
            }
        }
    }


    function get_result(result){
        if (result == "ERROR" || result == ""){
            console.log("No Media Files!")
        } else {
            var files = JSON.parse(result)
            media_files = files.output
            console.log("Media Files (" + media_files.length + ") : " + media_files)
            if (media_files.length > 0){
                media_mode.setIndex(0);
            } else{
                console.log("Cannot Play Media!")
            }
        }
    }


    // Play Multiple Videos
    Rectangle {
        id: media_mode
        x: 0
        y: 0
        width: parent_root.width
        height: parent_root.height
        visible: (mode=="mediaPlayer") ? true : false
        color: "black"
        anchors.horizontalCenter: parent_root.horizontalCenter
        anchors.verticalCenter: parent_root.verticalCenter

        function setIndex(i){
            index = i;
            index %= media_files.length;
            player.source = img_path_ + media_files[index];
            player.play();
            slot_handler.start_post_tvclog(media_files[index]);
        }

        function next(){
            setIndex(index + 1);
        }

        function previous(){
            setIndex(index - 1);
        }

        Connections {
            target: player
            onStopped: {
                if (player.status == MediaPlayer.EndOfMedia) {
                    if (index==media_files.length-1){ //Looping start from beginning
                        media_mode.setIndex(0);
                    } else{
                        media_mode.next();
                    }
                }
            }
        }

        MediaPlayer {
            id: player
        }

        VideoOutput {
            anchors.fill: media_mode
            source: player
        }

        MouseArea{
            anchors.fill: parent
            onDoubleClicked: {
                set_height = "normal"
                root.my_stack_view_visible = true
                root.reference = "people_count"
                media_mode.anchors.horizontalCenter = parent_root.horizontalCenter
                media_mode.anchors.verticalCenter = parent_root.verticalCenter
                welcome_window.visible = true
                show_timer_loading.start()
            }
        }

    }

    // Play Static Single Video
    /*Video {
        id: video
        visible: (mode=="staticVideo") ? true : false
        anchors.fill: parent
        source: img_path + "sample_1.mp4"
        focus: true

        MouseArea{
            anchors.fill: video
            onClicked: {
                if (video.playbackState == MediaPlayer.PlayingState) {
                    video.pause()
                } else {
                    video.play()
                }
            }
        }
    }*/

    //ViewFinder from Camera
   /* Item {
        visible: (mode=="liveView") ? true : false
        width: 1023
        height: 768

        Camera {
            id: camera
            imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
            exposure {
                exposureCompensation: -1.0;
                exposureMode: Camera.ExposurePortrait;
            }

            imageCapture {
                onImageCaptured: {
                    // Show the preview in an Image
                    photoPreview.source = preview;
                    photoPreview.visible = true
                }
            }
        }

        VideoOutput {
            id: vo
            source: camera
            focus : visible // to receive focus and capture key events when visible
            anchors.fill: parent

            MouseArea {
                id: voMouse
                visible: vo.focus
                anchors.fill: parent;
                onClicked: {
                    camera.imageCapture.capture();
                    text.text = "Captured...";
                }
            }
        }

        Image {
            id: photoPreview
            fillMode: Image.PreserveAspectFit
            MouseArea{
                id: previewMouse
                visible: photoPreview.visible
                anchors.fill: photoPreview
                onClicked: {
                    if(photoPreview.visible == true){
                        photoPreview.visible = false;
                        vo.focus = true;
                        text.text = "Live View...";
                    }
                }
            }
        }

        Text{
            id: text
            x: 25
            y: 25
            color: "yellow"
            font.pointSize: 20
            font.italic: true
            font.family: "Verdana"
            text: "Live View..."
        }

    }*/


    Rectangle{
        id: welcome_window
        x: 66
        y: 129
        color: "white"
        anchors.horizontalCenterOffset: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        opacity: 0.9
        width: 700
        height: 400
        border.width: 0
        anchors.horizontalCenter: parent_root.horizontalCenter
        visible: false

        Text {
            id: please_welcome
            x: 137
            y: 30
            width: 400
            height: 350
            text: "Hi There, Welcome to PopBox Locker @ " + locker_name
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: welcome_window.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:35
            color:"#c50808"
        }

        Rectangle{
            id: timer_rec
            width: 10
            height: 10
            x:0
            y:0
            visible: false
            QtObject{
                id:time_loading
                property int counter
                Component.onCompleted:{
                    time_loading.counter = loading_time
                }
            }
            Timer{
                id:show_timer_loading
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    time_loading.counter -= 1
                    if(time_loading.counter < 0){
                        show_timer_loading.stop()
                        welcome_window.visible = false
//                        slot_handler.start_get_detection("face")
                    }
                }
            }
        }
    }

}

