import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: popsend_select_denom
    width: 1024
    height: 768
    property int timer_value: 60
    property var cust_name:""
    property var member_data

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('handle_popsend_member : ' + JSON.stringify(member_data))
            define_member(member_data)
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    function define_member(member_data){
        var info = JSON.parse(member_data)
        cust_name = info.member_name
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

    Text {
        id: greeting_text
        x: 37
        y: 115
        width: 200
        height: 40
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        text: 'Hi, ' + cust_name
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:24
    }

    Text {
        id: text_select_denom
        x: 0
        y: 170
        width: 500
        height: 60
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        text: qsTr("Please Choose Denom :")
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:30
    }

    Row{
        id: buttons
        y: 268
        width: 1000
        height: 250
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 2

        Image{
            id: button_25k
            width: 245
            height: 235
            scale: 0.8
            source: "img/popsend/25k_white.png"

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(popsend_express_info,{member_data: member_data, amount: "25000"})
                }
                onEntered: {
                    button_25k.source = "img/popsend/25k_red.png"
                }
                onExited: {
                    button_25k.source = "img/popsend/25k_white.png"
                }
            }
        }

        Image{
            id: button_50k
            width: 245
            height: 235
            scale: 0.8
            source: "img/popsend/50k_white.png"

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(popsend_express_info,{member_data: member_data, amount: "50000"})
                }
                onEntered: {
                    button_50k.source = "img/popsend/50k_red.png"
                }
                onExited: {
                    button_50k.source = "img/popsend/50k_white.png"
                }
            }
        }

        Image{
            id: button_100k
            width: 245
            height: 235
            scale: 0.8
            source: "img/popsend/100k_white.png"

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(popsend_express_info,{member_data: member_data, amount: "100000"})
                }
                onEntered: {
                    button_100k.source = "img/popsend/100k_red.png"
                }
                onExited: {
                    button_100k.source = "img/popsend/100k_white.png"
                }
            }
        }

        Image{
            id: button_200k
            width: 245
            height: 235
            scale: 0.8
            source: "img/popsend/200k_white.png"

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.push(popsend_express_info,{member_data: member_data, amount: "200000"})
                }
                onEntered: {
                    button_200k.source = "img/popsend/200k_red.png"
                }
                onExited: {
                    button_200k.source = "img/popsend/200k_white.png"
                }
            }
        }
    }

    Text {
        id: powered_text
        x: 228
        y: 638
        width: 250
        height: 40
        color: "#ffffff"
        text: qsTr("Powered by :")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 30
        font.family: "Microsoft YaHei"
    }

    Rectangle {
        id: rec_emoney
        x: 488
        y: 619
        width: 300
        height: 77
        color: "#ffffff"

        Image {
            id: image_emoney
            x: 0
            y: 0
            width: 300
            height: 77
            source: "img/otherservice/logo-emoney300.png"
        }
    }

}
