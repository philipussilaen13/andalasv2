import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:mm_return_info
    property int timer_value: 30
    //property var initurl

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    FullWidthReminderText{
        id:mm_return_info_title
        x: 0
        y:143
        border.width: 0
        remind_text:qsTr("How To Return Parcel")
        remind_text_size:"45"
    }

    Image {
        id: step1
        x: 0
        y: 195
        width: 350
        height: 350
        source: "img/mminfo/mm_step1.png"
    }

    Image {
        id: step2
        x: 337
        y: 195
        width: 350
        height: 350
        source: "img/mminfo/mm_step2.png"
    }

    Image {
        id: step3
        x: 668
        y: 195
        width: 350
        height: 350
        source: "img/mminfo/mm_step3.png"
    }


    Text {
        id: textstep1
        x: 37
        y: 541
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Fill out Return Form on Mataharimall.com")
        textFormat: Text.AutoText
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
        font.italic: true
        font.family:"Microsoft YaHei"
        wrapMode: Text.WordWrap
    }

    Text {
        id: textstep2
        x: 374
        y: 541
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Go to locker, Select Return Parcel and Scan the AWB Number.")
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
        font.italic: true
        font.family:"Microsoft YaHei"
        wrapMode: Text.WordWrap
    }

    Text {
        id: textstep3
        x: 705
        y: 541
        width: 276
        height: 125
        color: "#ffffff"
        text: qsTr("Drop off the returned parcel to opened door.")
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
        font.italic: true
        font.family:"Microsoft YaHei"
        wrapMode: Text.WordWrap
    }

    Image{
        id:next_button
        x: 400
        y: 664
        width:250
        height:60
        source:"img/button/1.png"
        Text{
            text:qsTr("Return My Parcel")
            font.bold: false
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"red"
            font.pixelSize:28
            anchors.centerIn: parent;
        }
        MouseArea {
            y: 0
            width: 235
            height: 60
            enabled: true
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            onClicked: {
                slot_handler.start_customer_reject_select_merchant("MATAHARIMALL")
                my_stack_view.push(reject_input_memory_page)
            }
            onEntered:{
                next_button.source = "img/button/DeleteButton.png"
            }
            onExited:{
                next_button.source = "img/button/1.png"
            }
        }
    }

    
    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

}
