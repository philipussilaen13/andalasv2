import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQml 2.0

Background{
    id:qr_payment
    height: 768
    width: 1024
    img_vis: true
    property int timer_value: 120
    property var press: "0"
    property var products: ""
    property var customers: ""
    property var provider: ""
    property var global_trans: ""
    property url app_url: ""
    property bool small_logo_vis: false
    property var prod_name: "---"
    property var amount: "999000"
    property bool paymentSuccess: false
    property var prod_trans_id: "undefined"
    property var prod_valid_time: "1970-12-12 00:00:00"
    property var prod_pay_id: ""
    property var product_class: ""
    property var selected_locker: ""
    property var popsafe_param: undefined
    property bool triggerOpen: false
    property bool paymentCheck: true


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            loadingGif.open()
            main_page.enabled = false
            triggerOpen = false
            
            console.log("provider-terpilih: " + provider)
            
            define_provider(provider)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.check_trans_global_result.connect(trans_global_result);
        root.choose_mouth_result.connect(handle_text_mouth);
        root.customer_take_express_result.connect(process_result)
    }

    Component.onDestruction: {
        root.check_trans_global_result.disconnect(trans_global_result);
        root.choose_mouth_result.disconnect(handle_text_mouth);
        root.customer_take_express_result.disconnect(process_result)
    }

    function process_result(text){
        console.log('process_result : ' + text)
        switch(text){
            case "Success" : my_stack_view.push(customer_take_express_opendoor_view);
                break;
            case "Overdue" : my_stack_view.push(customer_take_express_overtime_view);
                break;
            case "Overdue||Popsafe" :
                my_stack_view.push(customer_take_express_overtime_view, {isPopDeposit: true});
                break;
            case "Error" : my_stack_view.push(customer_take_express_error_view);
                break;
            default : my_stack_view.push(customer_take_express_error_view);
        }
    }

    function handle_text_mouth(mouth){
        console.log("handle_text_mouth : ", mouth)
        if(mouth == 'Success' && triggerOpen == false){
            triggerOpen = true
            my_stack_view.push(send_door_open_page,{isPopDeposit: true, type:"none",store_type:"store",change_press:3,popsafe_param:popsafe_param})
        }
    }

    function trans_global_result(result){
        console.log("trans_global_result is ", result)
        if(result=="") return;
        var result_yap = JSON.parse(result)
        var result_trans = result_yap.status
        console.log(result_trans);
        if(result_trans=="PAID" && paymentCheck==true){
            paymentSuccess = true
            paymentCheck = false
            release_product(product_class);
        } 
        
        console.log(" payment status: " + paymentCheck)
        main_page.enabled = false
        yap_status_notif.visible = true
        confirm_yap.visible = false
    }

    function release_product(pc){
        switch(pc){
        case "sepulsa":
            var s = JSON.parse(products);
//            phone_no+'|'+prod_id_+'|'+butt_type_
            var i = s.sepulsa.split('|')
            var p = provider+'|'+prod_trans_id+'|'+amount
            slot_handler.start_sepulsa_transaction(i[0], i[1], i[2], p)
            break;
        case "popshop":
            var ps = JSON.parse(products);
//            prod_sku + "|" + prod_name + "|" + product_qty + "|" + total_amount + "|" + prod_qr_url
            //EXAMPLE
            //"product_info" : "PBSDIM00298|Mobile Phone Stand Suit - White|3|105000",
            //"customer_info" : "wahyudi|wahyudi@popbox.asia|085710157057",
            //"purchase_info" : "Simulator TestStation|6032984026023022|509277|emoney|2017-03-20 11:30",
            //"delivery_address" :"Grand Slipi Tower"
            var c = JSON.parse(customers)
            var cust = c.name+'|'+c.email+'|'+c.phone
            var now_string = Qt.formatDateTime(new Date(), "yyyy-MM-dd hh:mm")
            var payment = c.locker+'|'+prod_trans_id+'|'+amount+'|'+provider
            slot_handler.start_popshop_transaction(ps.popshop, cust, payment, selected_locker);
            break;
        case "popsafe-order":
            if (popsafe_param != undefined){
                var lockerSize = JSON.parse(popsafe_param).lockerSize
                slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
            }
            break
        case "popsafe-extend":
            if (popsafe_param != undefined){
                var pp = JSON.parse(products)
                var cc = JSON.parse(customers)
                var param = JSON.stringify({"expressNumber": pp.sku, "userSession": cc.sessionId, "trxRemarks": provider+"|"+prod_trans_id+"|"+amount})
                slot_handler.start_extend_express(param)
                slot_handler.start_video_capture("popsafe-extend-"+pp.sku+'-'+pp.pin)
                slot_handler.customer_take_express(pp.pin)
            }
            break;
        default:
            return
        }
    }

    function general_data_qr(obj){
        var product = JSON.parse(obj)
        qr_image.source = product.qr_url
        prod_name = product.name
        amount = product.total_amount
        main_page.enabled = true
    }

    function global_data_qr(objA, objB){
        var product = JSON.parse(objA)
        prod_name = "(" + product.qty + " " + product.name + ")"
        amount = product.total_amount
        var yap = JSON.parse(objB)
        prod_trans_id = yap.transaction_id
        prod_pay_id = yap.payment_id
        prod_valid_time = yap.expired_datetime
        qr_image.source = yap.url
        main_page.enabled = true
    }
   //TODO Create more text info for yap_transaction
    /*        {
            "payment_id": "BNI-YAP180224105220YCRKL",
            "transaction_id": "LKR1802242252HUG",
            "status": "CREATED",
            "url": "http://paymentdev.popbox.asia/img/payment/yap/BNI-YAP180224105220YCRKL.png",
            "expired_datetime": "2018-02-25 22:52:20"
        }*/

    function define_provider(provider){
        console.log('define provider : ' + provider)
        if (provider=="") return;
        switch(provider){
            case 'DIMO-PAYBYQR':
                general_data_qr(products)
                app_url = "img/payment/paybyqr-small.png"
                small_logo_vis = true
                break
            case 'BNI-YAP': case 'bni-yap':
                global_data_qr(products, global_trans)
                app_url = "img/payment/yap-small.png"
                break
            case 'GOPAY':
                global_data_qr(products, global_trans)
                app_url = "img/payment/gopay-small.png"
                break
            case 'TCASH':
                global_data_qr(products, global_trans)
                app_url = "img/payment/tcash-small.png"
                break
            case 'OTTO-QR':
                global_data_qr(products, global_trans)
                app_url = "img/payment/ottopay-small.png"
                break

            default:
                app_url = "img/otherImages/checked_white.png"
                break
        }

    }

    function insert_flg(a){
        if(a != undefined){
            var newstrA=""
            newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
            return newstrA
        }else{
            return a
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(qr_image.source!=undefined){
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                } else {
                    my_timer.stop()
                    my_stack_view.pop()
                }
            }
        }
    }

    Rectangle{
        id: main_page
        FullWidthReminderText{
            id:first_title
            x: 0
            y:139
            remind_text:qsTr("QR PAYMENT")
            remind_text_size:"35"
            visible: false
        }

        FullWidthReminderText{
            id:second_title
            x: 0
            y:150
            remind_text:qsTr("To Proceed, Kindly Scan This QR With :")
            remind_text_size:"25"
        }

        Image{
            id: qr_app_logo
            x: 750
            y: 166
            width: 100
            height: 80
            fillMode: Image.PreserveAspectFit
            source: app_url
        }

        Rectangle{
            id: rec_qr
            x: 362
            y: 232
            color: "white"
            radius: 20
            width: 300
            height: 300

            Image{
                id: qr_image
                scale: 0.95
                opacity: 1
                fillMode: Image.PreserveAspectFit
                anchors.fill: rec_qr
                onStatusChanged: if(qr_image.status == Image.Ready) loadingGif.close()
            }
        }

        Text{
            id:prod_item_name
            x: 362
            y: 549
            width: 300
            text: (provider=="BNI-YAP") ? prod_pay_id + " " + prod_name : prod_name
            color:"white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:18
            font.italic: true
        }

        Text{
            id:prod_item_price
            x: 212
            y: 579
            width: 600
            height: 80
            text: "Rp." + insert_flg(amount)
            wrapMode: Text.WordWrap
            color:"white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:30
            font.bold: true
        }

        Image {
            id: images_logos
            x: 32
            y: 644
            width: 961
            height: 101
            source: "img/payment/small_logos.png"
            visible: small_logo_vis
        }

        DoorButton{
            id:confirm_yap
            y:662
            x:373
            show_text:qsTr("Confirm")
            show_image:"img/door/1.png"
            visible: !small_logo_vis

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_check_trans_global()
                }
            }
        }

    }

    Rectangle{
        id:yap_status_notif
        x: 100
        y: 122
        width: 800
        height: 525
        visible:false
        color: "white"
        opacity: .90
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: yap_text_notif
            x: 93
            width: 850
            height: 50
            text: (paymentSuccess==true) ? qsTr("Congratulations, Your Payment is success.") : qsTr("The payment is still being confirmed.")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 78
            font.family:"Microsoft YaHei"
            color:"darkred"
            textFormat: Text.PlainText
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text{
            id:prod_item_trans_id
            x: 250
            width: 300
            text: prod_trans_id
            anchors.top: parent.top
            anchors.topMargin: 30
            font.bold: true
            color:"darkred"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:30
        }

        Image {
            id: yap_img_notif
            x: 410
            y: 235
            width: 215
            height: 215
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            source: (paymentSuccess==true) ? "img/payment/success_icon.png"  : "img/payment/failed-icon.png"
        }

        OverTimeButton{
            id:yap_notif_retry
            x:378
            y:589
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 25
            show_text: qsTr("Close")
            show_x:15
            bg_color: "red"
            show_source: ""
            text_color: "white"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(paymentSuccess==true){
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                    }else{
                        main_page.enabled = true
                        yap_status_notif.visible = false
                        confirm_yap.visible = true
                    }
                }
            }
        }
    }
    LoadingView{id:loadingGif}
}
