import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    width: 1024
    height: 768
    property int timer_value: 120
    property var press: "0"
    property var products: ""
    property var provider: ""
    property var global_trans: ""
    property url app_url: ""
    property bool small_logo_vis: false
    property var prod_name: "---"
    property var amount: "999000"
    property bool paymentSuccess: false
    property var prod_trans_id: "undefined"
    property var prod_valid_time: "1970-12-12 00:00:00"
    property var prod_pay_id: ""
    property var phone_number: ""
    property var paymentResult: undefined
    property var duration: undefined
    property bool testingMode: false
    property bool isExtending: false
    property var extend_pincode: undefined
    property var door_size: (testingMode==true) ? 'M' : 'L'


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value;
            my_timer.restart();
            press = "0";
            loadingPopUp.open();
            main_page.enabled = false;
            define_provider(provider);
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
        root.check_trans_global_result.connect(trans_global_result);
        root.choose_mouth_result.connect(select_door_result);
        root.apexpress_result.connect(next_process);
        root.update_apexpress_result.connect(process_update);
        root.customer_take_express_result.connect(process_result);
    }

    Component.onDestruction: {
        root.check_trans_global_result.disconnect(trans_global_result);
        root.choose_mouth_result.disconnect(select_door_result);
        root.apexpress_result.disconnect(next_process);
        root.update_apexpress_result.disconnect(process_update);
        root.customer_take_express_result.disconnect(process_result);
    }

    function process_result(text){
        console.log('process_result : ' + text)
        if (text=='Error'||text=='NotInput'){
            failure_notif.titleText = qsTr('Parcel Tidak Ditemukan');
            failure_notif.notifText = qsTr('Mohon Maaf, Silakan Masukkan Kode Pin Yang Benar.');
            failure_notif.open();
            return;
        }
        var status = text.split('||')[0];
        var info = text.split('||')[1];
        var detail = JSON.parse(info);
        var d = JSON.parse(detail.transactionType)
//        if (status=='Overdue'){
//            my_stack_view.push(select_payment_ap, {phone_number: detail.phone_number, isExtending: true, extendData: info});
//        }
        if (status=='Success'){
            my_stack_view.push(take_parcel_ap, {door_no: d.door_no});
            return;
        }
    }

    function process_update(u){
        console.log('process_update from trial ', getUpdateSignal, u);
        if (u=='Success'){
            slot_handler.customer_take_express(extend_pincode+'||AP_EXPRESS');
            slot_handler.start_video_capture("take_apexpress_" + extend_pincode);
            return;
        }
    }

    function next_process(t){
        console.log('next_process', t);
        loadingPopUp.close();
        if (t=='ERROR'){
            notif_text.contentNotif = qsTr('Terjadi Kesalahan, Silakan Hubungi Layanan Pelanggan.');
            notif_text.successMode = false;
            notif_text.open();
            return
        }
        var info = JSON.parse(t);
        var param = {
            'paymentType': 'bni-yap',
            'duration': duration,
            'amount': amount,
            'paymentStatus': info.result,
            'pin_code': info.pin_code,
            'date': info.date,
            'door_no': info.door_no,
            'result': paymentResult,
            'phone_number': phone_number,
            'express': info.express
        };
        my_stack_view.push(open_door_ap, {summary: JSON.stringify(param)});
        return
    }

    function select_door_result(r){
        console.log('select_door_result', r);
        switch (r){
        case 'NotMouth':
            notif_text.successMode = false;
            notif_text.contentNotif = qsTr('Mohon Maaf, Tidak Ada Loker Yang Tersedia Saat Ini.');
            notif_text.open();
            return;
        case 'Success':
//            slot_handler.get_express_mouth_number();
            slot_handler.start_store_apexpress(phone_number, duration);
            return;
        default:
            return;
        }
    }

    function trans_global_result(result){
        console.log("trans_global_result is ", result)
        if(result=="") return;
        var result_yap = JSON.parse(result);
        var result_trans = result_yap.status;
        if(result_trans=="PAID"){
            paymentSuccess = true;
            loadingPopUp.popupText = qsTr('Memproses...');
            if (isExtending){
                slot_handler.start_update_apexpress(extend_pincode);
            } else {
                slot_handler.start_choose_mouth_size(door_size,"staff_store_express");
            }
            return
        } else {
            loadingPopUp.close();
            main_page.enabled = false;
            yap_status_notif.visible = true;
            confirm_yap.visible = false;
            press = '0'
        }
    }

    function general_data_qr(obj){
        var product = JSON.parse(obj)
        qr_image.source = product.qr_url
        prod_name = product.name
        amount = product.total_amount
        main_page.enabled = true
    }

    function global_data_qr(objA, objB){
        var product = JSON.parse(objA)
        prod_name = "(" + product.name + ")"
        amount = product.total_amount
        var yap = JSON.parse(objB)
        prod_trans_id = yap.transaction_id
        prod_pay_id = yap.payment_id
        prod_valid_time = yap.expired_datetime
        qr_image.source = yap.url
        main_page.enabled = true
    }
   //TODO Create more text info for yap_transaction
    /*        {
            "payment_id": "BNI-YAP180224105220YCRKL",
            "transaction_id": "LKR1802242252HUG",
            "status": "CREATED",
            "url": "http://paymentdev.popbox.asia/img/payment/yap/BNI-YAP180224105220YCRKL.png",
            "expired_datetime": "2018-02-25 22:52:20"
        }*/

    function define_provider(provider){
        console.log('define provider : ' + provider)
        if (provider=="") return;
        switch(provider){
            case 'DIMO-PAYBYQR':
                general_data_qr(products)
                app_url = "img/payment/paybyqr-small.png"
                small_logo_vis = true
                break
            case 'BNI-YAP': case 'bni-yap':
                global_data_qr(products, global_trans)
                app_url = "img/payment/yap-small.png"
                break
            default:
                app_url = "img/otherImages/checked_white.png"
                break
        }

    }

    function insert_flg(a){
        if(a != undefined){
            var newstrA=""
            newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
            return newstrA
        }else{
            return a
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: main_page
        width: parent.width
        height: parent.height - 90
        color: "#b0b0b4"
        anchors.top: parent.top
        anchors.topMargin: 90

        Text {
            id: main_command_text
            width: 500
            height: 100
            color: "white"
            text: qsTr("Silakan Pindai QR Berikut")
            anchors.top: parent.top
            anchors.topMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: false
            font.family: 'Microsoft YaHei'
            font.pixelSize: 40

        }

        Image{
            id: qr_app_logo
            x: 668
            y: 79
            width: 100
            height: 80
            fillMode: Image.PreserveAspectFit
            source: app_url
        }

        Rectangle{
            id: rec_qr
            x: 362
            color: "white"
            radius: 20
            anchors.top: parent.top
            anchors.topMargin: 125
            anchors.horizontalCenter: parent.horizontalCenter
            width: 300
            height: 300

            Image{
                id: qr_image
                scale: 0.95
                opacity: 1
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
                onStatusChanged: if(qr_image.status == Image.Ready) loadingPopUp.close()
            }
        }

        Text{
            id:prod_item_name
            x: 362
            width: 300
            text: (provider=="BNI-YAP") ? prod_pay_id + " " + prod_name : prod_name
            anchors.top: parent.top
            anchors.topMargin: 450
            anchors.horizontalCenter: parent.horizontalCenter
            color:"white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:18
            font.italic: true
        }

        Text{
            id:prod_item_price
            x: 212
            width: 600
            height: 80
            text: "Rp." + insert_flg(amount)
            anchors.top: parent.top
            anchors.topMargin: 470
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            color:"white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:30
            font.bold: true
        }

        Image {
            id: images_logos
            x: 32
            y: 554
            width: 961
            height: 101
            source: "img/payment/small_logos.png"
            visible: small_logo_vis
        }

        NotifButtonAP{
            id: confirm_yap
            x: 365
            anchors.top: parent.top
            anchors.topMargin: 560
            anchors.horizontalCenter: parent.horizontalCenter
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('KONFIRMASI')
            visible: !small_logo_vis

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press!="0") return
                    press = "1"
                    loadingPopUp.popupText = qsTr('Memeriksa Pembayaran...');
                    loadingPopUp.open();
                    slot_handler.start_check_trans_global()
//                    console.log('confirm_button is pressed..!')
                }
            }
        }

    }

    Rectangle{
        id:yap_status_notif
        x: 100
        y: 122
        width: 800
        height: 525
        visible:false
        color: "white"
        opacity: .90
        radius: 22
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: yap_text_notif
            x: 93
            width: 850
            height: 50
            text: (paymentSuccess==true) ? qsTr("Selamat, Pembayaran Anda berhasil.") : qsTr("Pembayaran Anda Belum Terkonfirmasi.")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 78
            font.family:"Microsoft YaHei"
            color:"#009be1"
            textFormat: Text.PlainText
            font.pixelSize: 30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text{
            id:prod_item_trans_id
            x: 250
            width: 300
            text: prod_trans_id
            anchors.top: parent.top
            anchors.topMargin: 30
            font.bold: true
            color:"#009be1"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize:30
        }

        Image {
            id: yap_img_notif
            x: 410
            y: 235
            width: 215
            height: 215
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            source: (paymentSuccess==true) ? "img/apservice/icon/sukses_biru.png"  : "img/payment/failed-icon.png"
        }

        NotifButtonAP{
            id: yap_notif_retry
            x: 365
            anchors.top: parent.top
            anchors.topMargin: 410
            anchors.horizontalCenter: parent.horizontalCenter
            buttonColor: 'BLUE'
            modeReverse: false
            buttonText: qsTr('COBA LAGI')

            MouseArea {
                visible: !isExtending
                anchors.fill: parent
                onClicked: {
                    main_page.enabled = true;
                    press = '0';
                    yap_status_notif.visible = false;
                    confirm_yap.visible = true;
                }
            }
//            MouseArea {
//                visible: isExtending
//                anchors.fill: parent
//                onClicked: {
//                    if (press != '0') return
//                    press = '1';
//                    if (paymentSuccess==true){
//                        loadingPopUp.open();
//                        slot_handler.start_update_apexpress(extend_pincode);
//                    }else{
//                        main_page.enabled = true;
//                        press = '0';
//                    }
//                    yap_status_notif.visible = false;
//                    confirm_yap.visible = true;
//                }
//            }
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
        successMode: false
    }

    LoadingPopUp{
        id: loadingPopUp
        z: 99
    }}
