import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

ComboBox {
    id: root
    width: 200
    height: 50

    property alias comboBoxModel: root.model
    signal indexChanged()
    property alias currentIndex: root.currentIndex
    property alias currentText: root.currentText

    property Component comboBoxStyleBackground: Component { Rectangle{} }
    property Component dropDownMenuStyleFrame: Component { Rectangle{} }

    function setComboBoxStyleBackground(background) {
        comboBoxStyleBackground = background
    }

    function setDropDownMenuStyleFrame(frame) {
        dropDownMenuStyleFrame = frame
    }

    model: ListModel {
        id: cbItems
        ListElement { text: "" }
    }

//    model: ["A", "B", "C", "D", "E"]

    style: ComboBoxStyle {
        id: comboBoxStyle
        background: comboBoxStyleBackground
        label: Text {
            color: "black"
            width: root.width
            height: root.height
            text: control.currentText
        }

        __dropDownStyle: MenuStyle {
            id: dropDownMenuStyle
            frame: dropDownMenuStyleFrame
            itemDelegate.label: Text {
                width:root.width - 50
                height: root.height
                color: styleData.selected ? "blue" : "black"
                text: styleData.text
            }

            itemDelegate.background: Rectangle {
                z: 1
                opacity: 0.5
                color: styleData.selected ? "darkGray" : "transparent"
            }
        }
    }

    onCurrentIndexChanged: {
        root.indexChanged()
    }
}
