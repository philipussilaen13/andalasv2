import QtQuick 2.4
import QtQuick.Controls 1.2

BackgroundTakeExpress{
    id:user_take_memory_input
    width: 1024
    height: 768
    property var press:"0"
    property int timer_value: 60
    property var show_text:""
    property var show_text_count:0
    property bool isPopDeposit: false

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            if(first_validate_code_box.show_text != ""){
                first_validate_code_box.show_text = ""
                second_validate_code_box.show_text = ""
                third_validate_code_box.show_text = ""
                fourth_validate_code_box.show_text = ""
                fifth_validate_code_box.show_text = ""
                sixth_validate_code_box.show_text = ""
                touch_keyboard.count = 0
            }
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_customer_scan_qr_code()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            slot_handler.stop_customer_scan_qr_code()
        }
    }


    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
            }
    }

    FullWidthReminderText{
            id:text
            y:140
            remind_text:qsTr("Scan or Enter Barcode")
            remind_text_size:"35"
        }

    ValidateCodeBox{
        id:first_validate_code_box
        x:345
        y:203
    }

    ValidateCodeBox{
        id:second_validate_code_box
        x:401
        y:203
    }

    ValidateCodeBox{
        id:third_validate_code_box
        x:457
        y:203
    }

    ValidateCodeBox{
        id:fourth_validate_code_box
        x:513
        y:203
    }

    ValidateCodeBox{
        id:fifth_validate_code_box
        x:569
        y:203
    }

    ValidateCodeBox{
        id:sixth_validate_code_box
        x:625
        y:203
    }

    Image{
        x: 412
        y: 280
        width:199
        height:64
        source:"img/button/barcode.png"
    }


    Component.onCompleted: {
        root.customer_take_express_result.connect(process_result)
    }

    Component.onDestruction: {
        root.customer_take_express_result.disconnect(process_result)
    }

    function process_result(text){
        console.log('process_result : ' + text)
        if (text.indexOf('PopDeposit_Extend') > -1){
            var express_data = text.split('||')[1]
            isPopDeposit = true;
            my_stack_view.push(customer_take_express_overtime_view, {isPopDeposit: isPopDeposit, expressData: express_data});
            return
        } else {
            switch(text){
                case "Success" : my_stack_view.push(customer_take_express_opendoor_view);
                    break;
                case "Overdue" : my_stack_view.push(customer_take_express_overtime_view);
                    break;
                case "Overdue||Popsafe" :
                    isPopDeposit = true;
                    my_stack_view.push(customer_take_express_overtime_view, {isPopDeposit: isPopDeposit});
                    break;
                case "Error" : my_stack_view.push(customer_take_express_error_view);
                    break;
                default : my_stack_view.push(customer_take_express_error_view);
            }
        }
    }

Rectangle{
    id:main_page
    FullKeyboard{
        id:touch_keyboard
        x:29
        y:360
        property var count:0
        property var validate_code:""

        Rectangle{
            QtObject{
                id:abc
                property int counter
                Component.onCompleted:{
                    abc.counter = timer_value
                }
            }

            Timer{
                id:my_timer
                interval:1000
                repeat:true
                running:true
                triggeredOnStart:true
                onTriggered:{
                    abc.counter -= 1
                    if(abc.counter < 0){
                        my_timer.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }

        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code)
            touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            root.start_customer_scan_qr_code_result.connect(read_scanner)

        }

        Component.onDestruction: {
            touch_keyboard.letter_button_clicked.disconnect(show_validate_code)
            touch_keyboard.function_button_clicked.disconnect(on_function_button_clicked)
            root.start_customer_scan_qr_code_result.disconnect(read_scanner)
        }

        function read_scanner(text){
            var result = text
            console.log('read_scanner_result : ' + result + ', with length : ' + result.length)
            if(result == "" || result.length != 6){
                return
            }
            first_validate_code_box.show_text = result.substring(0,1)
            second_validate_code_box.show_text = result.substring(1,2)
            third_validate_code_box.show_text = result.substring(2,3)
            fourth_validate_code_box.show_text = result.substring(3,4)
            fifth_validate_code_box.show_text = result.substring(4,5)
            sixth_validate_code_box.show_text = result.substring(5,6)
            slot_handler.start_video_capture("take_express_" + result)
            slot_handler.customer_take_express(result)

        }

        function on_function_button_clicked(str){
            if(str == "ok"){
                if(press != "0"){
                    return
                }
                press="1"
                my_timer.stop()
                validate_code = ""
                validate_code += first_validate_code_box.show_text
                validate_code += second_validate_code_box.show_text
                validate_code += third_validate_code_box.show_text
                validate_code += fourth_validate_code_box.show_text
                validate_code += fifth_validate_code_box.show_text
                validate_code += sixth_validate_code_box.show_text
                slot_handler.start_video_capture("take_express_" + validate_code)
                slot_handler.customer_take_express(validate_code)
            }
        }

        function show_validate_code(str){
            if (str == "" && count > 0){
                count--
            }
            if (count == 0){
                first_validate_code_box.show_text = str
            }
            if (count == 1){
                second_validate_code_box.show_text = str
            }
            if (count == 2){
                third_validate_code_box.show_text = str
            }
            if (count == 3){
                fourth_validate_code_box.show_text = str
            }
            if (count == 4){
                fifth_validate_code_box.show_text = str
            }
            if (count == 5){
                sixth_validate_code_box.show_text = str
            }
            if (str != "" && count < 6){
                count++
            }
            abc.counter = timer_value
            my_timer.restart()
        }
    }
}

}


