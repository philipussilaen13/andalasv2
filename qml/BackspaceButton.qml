import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:86
    height:80
    color:"#5a5a5a"
    radius: 22

    Image{
        id:backspace_button_image
        width:86
        height:80
        source:""
    }

    Text{
        text:qsTr("backspace")
        color:"#ffffff"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            my_stack_view.pop()
        }
    }
}
