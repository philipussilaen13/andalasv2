import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:279
    height:64
    color:"transparent"

    property var show_text:""
    property var show_source:"img/bottondown/buttondown.png"

    Image{
        id:select_language_button
        width:279
        height:64
        source:show_source
    }
    Rectangle{
        y:14
        width:275
        height:35
        color:"transparent"
        Text{
            font.family:"Microsoft YaHei"
            text:show_text
            color:"red"
            font.pixelSize:35
            anchors.centerIn: parent;
        }
    }

}
