import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:reject_keyboard
    width:268
    height:358
    color:"transparent"
    signal letter_button_clicked(string str)
    signal function_button_clicked(string str)



    RejectNumButton{
        x:0
        y:0
        show_text:"1"
    }
    RejectNumButton{
        x:90
        y:0
        show_text:"2"
    }
    RejectNumButton{
        x:180
        y:0
        show_text:"3"
    }
    RejectNumButton{
        x:0
        y:90
        show_text:"4"
    }
    RejectNumButton{
        x:90
        y:90
        show_text:"5"
    }
    RejectNumButton{
        x:180
        y:90
        show_text:"6"
    }
    RejectNumButton{
        x:0
        y:180
        show_text:"7"
    }
    RejectNumButton{
        x:90
        y:180
        show_text:"8"
    }
    RejectNumButton{
        x:180
        y:180
        show_text:"9"
    }


    RejectNumButton{
        x:90
        y:270
        show_text:"0"
    }

    RejectNumboardFunctionButton{
        x:180
        y:270
        slot_text:"ok"
        show_image:"img/button/OkButton.png"
    }

    RejectNumboardFunctionButtondelete{
        x:0
        y:270
        slot_text:"delete"
       // show_image:"img/button/DeleteButton.png"
    }
}
