import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property int timer_value: 180
    property var press:"0"
    property var id
    property var phone_number
    property var phone_text
    property var phone
    property var send_time
    property var store_time
    property var number:8
    property var pay
    property var send_id
    property var remark
    property var save_time
    property var address
    property var time
    property var num
    property var check_list: new Array()
    property var page_num
    property var press_time:1
    property var show_express_id
    property var send_express_data
    property var send_express_num
    property var electronic_commerce
    property var electronic_commerce_

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_customer_load_reject_express_list(1)
            slot_handler.start_load_customer_reject_express_count()
            up_button.visible = false
            up_button.enabled = false
            down_button.enabled = false
            down_button.visible = false
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    function count_page(send_express_num){
        if(send_express_num/5 > Math.floor(send_express_num/5)){
            page_num = Math.floor(send_express_num/5)+1
        }
        else
            page_num = send_express_num/5
    }

    focus:true


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    PickUpButton{
        id:pick_up_Button1
        y:620
        x:360
        show_text:qsTr("take back it")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press="1"
                pick_up_Button1.enabled = false
                pick_up_Button2.enabled = false
                page.enabled = false
                waiting_for_result.open()
                slot_handler.start_courier_take_reject_express(JSON.stringify(check_list))
            }
        }
    }

    PickUpButton{
        id:pick_up_Button2
        y:620
        x:660
        show_text:qsTr("take back all")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press="1"
                pick_up_Button1.enabled = false
                pick_up_Button2.enabled = false
                page.enabled = false
                waiting_for_result.open()
                slot_handler.start_courier_take_reject_express_all()
            }
        }
    }

    Rectangle{
        Image {
            id: image1
            x: 60
            y: 161
            width: 880
            height: 45
            source: "img/courier19/one_1.png"
        }
        Row {
            id: row1
            x: 60
            y: 150
            width: 880
            height: 69

            Rectangle{
                Text{
                    font.family:"Microsoft YaHei"
                    text:qsTr("id")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 150
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{

                    font.family:"Microsoft YaHei"
                    text:qsTr("store-time")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:400
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{

                    font.family:"Microsoft YaHei"
                    text:qsTr("COMPANY")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:600
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{

                    font.family:"Microsoft YaHei"
                    text:qsTr("box")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 800
                    color:"red"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }
        }
    }

    Item  {
        id: page
        x:60
        y:220
        width: 870
        height:300

        ListView{
            id:listView
            anchors.fill:parent
            model:model
            delegate:listDel
            snapMode:ListView.SnapOneItem
        }

        ListModel {
            id: model
        }

        ListView {
            id: view
            anchors.fill: parent
            model: model
            delegate: delegate
            spacing:0
            interactive:false
        }

        Component {
            id: listDel

            Rectangle {
                height: 60

                Image {
                    id: image1
                    y:60
                    width: 880
                    height: 1
                    source: "img/courier19/stripe.png"
                }

                CheckBox {
                    id: checkbox1
                    x:20
                    width:60
                    height:60

                    onCheckedChanged: {
                        abc.counter = timer_value
                        my_timer.restart()
                        if (checked) {
                            check_list.push(show_express_id)
                        } else {
                            for(var i in check_list){
                                if(show_express_id == check_list[i]){
                                    check_list.splice(i,1)
                                }
                            }
                        }
                    }
                }

                Text{
                    font.family:"Microsoft YaHei"
                    text:username
                    anchors.horizontalCenterOffset:150
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    font.family:"Microsoft YaHei"
                    text:save_time
                    anchors.horizontalCenterOffset: 400
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    font.family:"Microsoft YaHei"
                    text:electronic_commerce
                    anchors.horizontalCenterOffset: 600
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }


                Text{
                    font.family:"Microsoft YaHei"
                    text:number
                    anchors.horizontalCenterOffset: 800
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    font.family:"Microsoft YaHei"
                    text:show_express_id
                    visible:false
                    anchors.horizontalCenterOffset: 810
                    color:"#ffffff"
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
            }
        }
    }


    function show_send_express(send_express_data){
        if(send_express_data == ""){
            return
        }
        var obj = JSON.parse(send_express_data)
        for(var i in obj){

            send_id = obj[i].customerStoreNumber
            time = obj[i].storeTime

            var date = new Date(time);
            var time_Y = date.getFullYear() + '-';
            var time_M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
            var time_D = date.getDate() + ' ';
            var time_h = date.getHours() + ':';
            var time_m = date.getMinutes() + ':';
            var time_s = date.getSeconds();
            store_time = time_Y+time_M+time_D+time_h+time_m+time_s

            num = obj[i].mouth.number

            electronic_commerce_ = obj[i].electronicCommerce.name

            id = obj[i].id

            model.append({"electronic_commerce":electronic_commerce_, "username":send_id,"save_time": store_time,"number": num,"show_express_id":id});
        }
    }

    Rectangle{
        id:up_button
        y:540
        x:60
        width:40
        height:40
        color:"transparent"

        Image{
            width:40
            height:40
            source:"img/05/back_red.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()
                model.clear()
                check_list = new Array()
                send_express_data = ""
                down_button.visible = true
                down_button.enabled = true
                press_time = press_time-1
                slot_handler.start_customer_load_reject_express_list(press_time)
                if(press_time == 1){
                    up_button.visible = false
                    up_button.enabled = false
                }
            }
        }
    }

   Rectangle{
        id:down_button
        y:540
        x:660
        width:40
        height:40
        color:"transparent"
        anchors.right: parent.right
        anchors.rightMargin: 80

        Image{
            width:40
            height:40
            source:"img/05/ok_red.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()                
                model.clear()
                check_list = new Array()
                send_express_data = ""
                up_button.visible = true
                up_button.enabled = true
                press_time = press_time+1
                slot_handler.start_customer_load_reject_express_list(press_time)
                if(press_time == page_num){
                    down_button.visible = false
                    down_button.enabled = false
                }
            }
        }
    }


   HideWindow{
       id:waiting_for_result
       //visible: true

       Image {
           id: img_time_waiting
           x: 437
           y: 400
           width: 150
           height: 200
           source: "img/otherImages/loading.png"
       }

       Text {
           x: 0
           y:300
           width: 1024
           height: 60
           text: qsTr("waiting")
           font.family:"Microsoft YaHei"
           color:"#FFFFFF"
           textFormat: Text.PlainText
           font.pointSize:45
           horizontalAlignment: Text.AlignHCenter
           verticalAlignment: Text.AlignBottom
       }
   }

    HideWindow{
        id:set_null
        //visible:true

        Image {
            id: img_set_null
            x: 437
            y: 362
            width: 150
            height: 200
            source: "img/otherImages/checklist.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("set_null")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:set_null_back
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    model.clear()
                    pick_up_Button1.enabled = true
                    pick_up_Button2.enabled = true
                    page.enabled = true
                    slot_handler.start_customer_load_reject_express_list(1)
                    set_null.close()
                }
            }
        }
    }

    HideWindow{
        id:set_ok
        //visible: true

        Image {
            id: img_set_ok
            x: 437
            y: 362
            width: 150
            height: 200
            source: "img/otherImages/checklist.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Successful")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:set_ok_back
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    model.clear()
                    pick_up_Button1.enabled = true
                    pick_up_Button2.enabled = true
                    page.enabled = true
                    press_time = 1
                    up_button.visible = false
                    up_button.enabled = false
                    slot_handler.start_customer_load_reject_express_list(1)
                    slot_handler.start_load_customer_reject_express_count()
                    set_ok.close()
                }
            }
        }
    }

    HideWindow{
        id:set_failed
        //visible: true

        Image {
            id: img_set_failed
            x: 437
            y: 362
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            y:266
            width: 1024
            height: 60
            text: qsTr("Failed")
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:set_failed_back
            x:373
            y:598
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    model.clear()
                    pick_up_Button1.enabled = true
                    pick_up_Button2.enabled = true
                    page.enabled = true
                    press_time = 1
                    up_button.visible = false
                    up_button.enabled = false
                    slot_handler.start_customer_load_reject_express_list(1)
                    slot_handler.start_load_customer_reject_express_count()
                    set_failed.close()
                }
            }
        }
    }

    Component.onCompleted: {
        root.reject_express_list_result.connect(send_express_list)
        root.send_express_count_result.connect(send_count)
        root.take_send_express_result.connect(take_result)
    }

    Component.onDestruction: {
        root.reject_express_list_result.disconnect(send_express_list)
        root.send_express_count_result.disconnect(send_count)
        root.take_send_express_result.disconnect(take_result)
    }

    function take_result(text){
        waiting_for_result.close()
        if(text == 'Success'){
            check_list = new Array()
            set_ok.open()
        }
        else if(text == 'None'){
            check_list = new Array()
            set_null.open()
        }
        else{
            check_list = new Array()
            set_failed.open()
        }
    }

    function send_express_list(text){
        send_express_data = text
        show_send_express(text)
    }

    function send_count(text){
        send_express_num = text
        count_page(text)
        if(text<=5){
            down_button.enabled = false
            down_button.visible = false
        }
        else{
            down_button.enabled = true
            down_button.visible = true
        }
    }
}
