import QtQuick 2.4
import QtQuick.Controls 1.2
import "door_price.js" as DOORS

Background{
    id:customer_send_select_box
    property var boxSize_status:"FFFFF"
    property int timer_value: 60
    property var press: "0"
    property var store_type: "store"
    property var usageOf: undefined
    property int change_press:0
    property var lockerSize: ""
    property var customer_phone: ""
    property bool useSamePrice: true
    property bool isPopDeposit
    property var selected_locker: ""


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log("change_press [select_box] status :", change_press)
            slot_handler.start_get_locker_name()
            updateStatus()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            if(store_type == "change"){
                change_press += 1
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    function updateStatus(){
        if( boxSize_status.substring(4,5) == "T"){
            mini_box.show_source = "img/courier14/14ground.png"
            mini_box.show_image = "img/courier14/size1.png"
            mini_box.enabled = true
        }
        else{
            mini_box.show_source = "img/courier14/gray1.png"
            mini_box.enabled = false
        }
        if( boxSize_status.substring(3,4) == "T"){
            small_box.show_source = "img/courier14/14ground.png"
            small_box.show_image = "img/courier14/size2.png"
            small_box.enabled = true
        }
        else{
            small_box.show_source = "img/courier14/gray1.png"
            small_box.enabled = false
        }
        if( boxSize_status.substring(2,3) == "T"){
            middle_box.show_source = "img/courier14/14ground.png"
            middle_box.show_image = "img/courier14/size3.png"
            middle_box.enabled = true
        }
        else{
            middle_box.show_source = "img/courier14/gray1.png"
            middle_box.enabled = false
        }

        if( boxSize_status.substring(1,2) == "T"){
            big_box.show_source = "img/courier14/14ground.png"
            big_box.show_image = "img/courier14/size4.png"
            big_box.enabled = true
        }
        else{
            big_box.show_source = "img/courier14/gray1.png"
            big_box.enabled = false
        }

        if( boxSize_status.substring(0,1) == "T"){
            extra_big_box.show_source = "img/courier14/14ground.png"
            extra_big_box.show_image = "img/courier14/size5.png"
            extra_big_box.enabled = true
        }

        else{
            extra_big_box.show_source = "img/courier14/gray1.png"
            extra_big_box.enabled = false
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    if(store_type == "store"){
                        my_timer.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                    if(store_type == "change"){
                        my_timer.stop()
                        slot_handler.start_store_customer_express()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    function compile_popsafe_data(no){
        var price = DOORS.get_price('DEFAULT');
        if (!useSamePrice) price = DOORS.get_price(no)
        var customers = JSON.stringify({ "name": customer_phone, "phone": customer_phone, "email": customer_phone})
        var products = JSON.stringify({"sku": "PSDL", "name": "PopSafe Locker Order|" + no, "qty": "1", "total_amount": price, "lockerSize": no})
        my_stack_view.push(grocery_express_info, {customers: customers, products: products, usageOf: usageOf, selected_locker: selected_locker})
    }


    FullWidthReminderText{
        id:please_select_service
        y:150
        remind_text:qsTr("please select service option")
        remind_text_size:"35"
    }

        CourierSelectBoxSizeButton{
            id:mini_box
            x: 127
            y: 214
            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size1.png"

            MouseArea {
                id: mouseArea1
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    lockerSize = "MINI"
                    if (usageOf=='popsafe-order'){
                        compile_popsafe_data(lockerSize)
                    } else {
                        slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                    }
                }
                onEntered:{
                mini_box.show_image = "img/courier14/size6.png"
                }
                onExited:{
                mini_box.show_source = "img/courier14/14ground.png"
                mini_box.show_image = "img/courier14/size1.png"
                }
            }

            Text {
                    id: mini_num
                    x: 126
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
            }
        }


        CourierSelectBoxSizeButton{
            id:small_box
            x: 387
            y: 214
            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size2.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    lockerSize = "S"
                    if (usageOf=='popsafe-order'){
                        compile_popsafe_data(lockerSize)
                    } else {
                        slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                    }
                }
                onEntered:{
                    small_box.show_image = "img/courier14/size7.png"
                }
                onExited:{
                    small_box.show_source = "img/courier14/14ground.png"
                    small_box.show_image = "img/courier14/size2.png"
                }

                Text {
                    id: small_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:middle_box
            x: 647
            y: 214

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size3.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    lockerSize = "M"
                    if (usageOf=='popsafe-order'){
                        compile_popsafe_data(lockerSize)
                    } else {
                        slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                    }
                }
                onEntered:{
                    middle_box.show_image = "img/courier14/size8.png"
                }
                onExited:{
                    middle_box.show_source = "img/courier14/14ground.png"
                    middle_box.show_image = "img/courier14/size3.png"
                }

                Text {
                    id: mid_num
                    x: 126
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:big_box
            x: 127
            y: 424

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size4.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    lockerSize = "L"
                    if (usageOf=='popsafe-order'){
                        compile_popsafe_data(lockerSize)
                    } else {
                        slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                    }
                }
                onEntered:{
                    big_box.show_image = "img/courier14/size9.png"
                }
                onExited:{
                    big_box.show_source = "img/courier14/14ground.png"
                    big_box.show_image = "img/courier14/size4.png"
                }

                Text {
                    id: big_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }

        CourierSelectBoxSizeButton{
            id:extra_big_box
            x: 387
            y: 424

            show_source:"img/courier14/14ground.png"
            show_image:"img/courier14/size5.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    lockerSize = "XL"
                    if (usageOf=='popsafe-order'){
                        compile_popsafe_data(lockerSize)
                    } else {
                        slot_handler.start_choose_mouth_size(lockerSize,"customer_store_express")
                    }
                }
                onEntered:{
                    extra_big_box.show_image = "img/courier14/size10.png"
                }
                onExited:{
                    extra_big_box.show_source = "img/courier14/14ground.png"
                    extra_big_box.show_image = "img/courier14/size5.png"
                }

                Text {
                    id: extra_big_num
                    x: 127
                    y: 0
                    color: "#ff0000"
                    text: qsTr("0")
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    styleColor: "#ffffff"
                    horizontalAlignment: Text.AlignHCenter
                    font.family:"Microsoft YaHei"
                    font.bold: true
                    font.pixelSize: 24
                }
            }
        }
/*
    BackButtonMenu{
        id:select_box_back_button
        x:647
        y:533
        show_text:qsTr("back to menu")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                //my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 7) return true }))
            }
            onEntered:{
                select_box_back_button.show_source = "img/05/pushdown.png"
            }
            onExited:{
                select_box_back_button.show_source = "img/05/button.png"
            }
        }
    }
*/
    Component.onCompleted: {
        root.choose_mouth_result.connect(handle_text)
        root.mouth_status_result.connect(handle_mouth_status)
        root.free_mouth_result.connect(show_free_mouth_num)
        slot_handler.start_get_mouth_status()
        slot_handler.start_get_free_mouth_mun()
        root.start_get_locker_name_result.connect(show_locker_name)
    }

    Component.onDestruction: {
        root.choose_mouth_result.disconnect(handle_text)
        root.mouth_status_result.disconnect(handle_mouth_status)
        root.free_mouth_result.disconnect(show_free_mouth_num)
        root.start_get_locker_name_result.disconnect(show_locker_name)
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        selected_locker = text
    }

    function show_free_mouth_num(text){
        var obj = JSON.parse(text)
        for(var i in obj){
            if(i == "XL"){
                extra_big_num.text = obj[i]
            }
            if(i == "L"){
                big_num.text = obj[i]
            }
            if(i == "M"){
                mid_num.text = obj[i]
            }
            if(i == "S"){
                small_num.text = obj[i]
            }
            if(i == "MINI"){
                mini_num.text = obj[i]
            }
        }
    }

    function handle_text(text){
        if(text == 'Success'){
            my_stack_view.push(send_door_open_page,{isPopDeposit: isPopDeposit, type:"none",store_type:"store",containerqml:customer_send_select_box,change_press:change_press})
        }
        if(text == 'NotMouth'){
            select_box_back_button.enabled = false
            not_mouth.open()
        }
    }

    function handle_mouth_status(text){
        boxSize_status = text
        updateStatus()
    }

    function clickedfunc(temp){
        store_type = temp
    }


    HideWindow{
        id:not_mouth
        Text {
            y:350
            width: 1024
            height: 60
            text: qsTr("not_mouth")
            font.family:"Microsoft YaHei"
            color:"#444586"
            textFormat: Text.PlainText
            font.pointSize:45
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }

        OverTimeButton{
            id:not_mouth_back
            x:350
            y:560
            show_text:qsTr("back")
            show_x:15


            MouseArea {
                anchors.fill: parent
                onClicked: {
                    select_box_back_button.enabled = true
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    not_mouth.close()
                }
            }
        }
    }
}
