import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:88
    height:88
    color:"transparent"
    property var slot_text:""
    property var show_image:""

    Image{
        id:function_button_Image
        width:88
        height:88
        source:"img/button/KeyButton_2.png"
    }
    Text{
        text:qsTr("DEL")
        color:"red"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(slot_text != "delete"){
                reject_keyboard.function_button_clicked(slot_text)
            }
            else
                reject_keyboard.letter_button_clicked("")
        }
        onEntered:{
            function_button_Image.source = "img/button/Functiondown.png"
        }
        onExited:{
            function_button_Image.source = "img/button/KeyButton_2.png"
        }
    }

}
