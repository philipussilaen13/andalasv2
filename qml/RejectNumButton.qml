import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:88
    height:88
    color:"transparent"
    property var show_text:""

    Image{
        id:num_button
        width:88
        height:88
        source:"img/button/NumKeyButton_2.png"
    }

    Text{
        text:show_text
        color:"red"
        font.family:"Microsoft YaHei"
        font.pixelSize:24
        anchors.centerIn: parent;
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            reject_keyboard.letter_button_clicked(show_text)
        }
        onEntered:{
            num_button.source = "img/bottondown/numbuttondown1.png"
        }
        onExited:{
            num_button.source = "img/button/NumKeyButton_2.png"
        }
    }

}
