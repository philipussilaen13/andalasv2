import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property var change_press: "0"
    property var store_type:"store"
    property variant containerqml: null
    property var press_timer_button: 0
    property bool change_door_status: false


    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            confirm_button.enabled = true
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Timer{
        id:press_timer
        interval:3000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            press_timer_button = 0
            press_timer.stop()
        }
    }

    Component.onCompleted: {
        root.mouth_number_result.connect(show_text)
        slot_handler.get_express_mouth_number()
    }

    Component.onDestruction: {
        root.mouth_number_result.disconnect(show_text)
    }

    function show_text(t){
        mouth_number.text = t
    }

    function clickedfunc(temp){
        containerqml.clickedfunc(temp)
    }

    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Please ensure compartment door closure after deposit")
        remind_text_size:"24"
    }

    DoorEvey{
        y:251
        x:263
    }

    DoorButton {
        id:change_box_button
        x: 88
        y: 640
        width: 279
        height:64
        show_text: qsTr("change-box")
        show_image:"img/door/1.png"
        visible: change_door_status

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(change_press != "0"){
                    return
                }
                change_press = "1"
                containerqml.clickedfunc("change")
                my_stack_view.pop()
            }
            onEntered:{
                change_box_button.show_image = "img/door/2.png"
            }
            onExited:{
                change_box_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:open_again_button
        y:640
        x:373
        show_text:qsTr("Open again")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press_timer_button == 0){
                    abc.counter = timer_value
                    my_timer.restart()
                    slot_handler.start_open_mouth_again()
                    press_timer.start()
                    open_again_button.enabled = false
                    press_timer_button = 1
                    press = "0"
                    confirm_button.enabled = true
                }
            }
            onEntered:{
                open_again_button.show_image = "img/door/2.png"
            }
            onExited:{
                open_again_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id: confirm_button
        y:640
        x:657
        show_text:qsTr("Confirm receipt")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                slot_handler.start_store_customer_reject_for_electronic_commerce()
                my_stack_view.push(reject_express_thank_page)
            }
            onEntered:{
                confirm_button.show_image = "img/door/2.png"
            }
            onExited:{
                confirm_button.show_image = "img/door/1.png"
            }
        }
    }

    Row {
        id: row1
        x: 272
        y: 130

        Text {
            id: box_open_tips_1
            x: 0
            color: "#FFFFFF"
            text: qsTr("Your locker is located at Box No ")
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }

        Text {
            id:mouth_number
            color: "#FFFFFF"
            text: qsTr("X")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }
    }

    Column {
        x: 315
        y: 175
        spacing: 5

        Row {
            spacing: 10

            Text {
                color: "#FFFFFF"
                text: qsTr("The system will refresh in")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }

            Rectangle{
                width:39
                height:11
                y:10
                color:"transparent"
                QtObject{
                    id:abc
                    property int counter
                    Component.onCompleted:{
                        abc.counter = timer_value
                    }
                }

                Text{
                    id:countShow_test
                    x:183
                    y:500
                    anchors.centerIn:parent
                    color:"#FFFFFF"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }

                Timer{
                    id:my_timer
                    interval:1000
                    repeat:true
                    running:true
                    triggeredOnStart:true
                    onTriggered:{
                        countShow_test.text = abc.counter
                        abc.counter -= 1
                        if(abc.counter <= 1){
                            confirm_button.enabled = false
                        }
                        if(abc.counter < 0){
                            my_timer.stop()
                            my_stack_view.push(reject_express_thank_page)
                            slot_handler.start_store_customer_reject_for_electronic_commerce()
                        }
                    }
                }
            }

            Text {
                color: "#FFFFFF"
                text: qsTr("sec")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }
        }
    }
}
