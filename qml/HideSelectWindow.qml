import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:hide_select_button_window

    visible: false

    function open(){
        hide_select_button_window.visible = true
    }
    function close(){
        hide_select_button_window.visible = false
    }
}
