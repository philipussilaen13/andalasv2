import QtQuick 2.4
import QtQuick.Controls 1.2



Rectangle{

    width:187
    height:91
    color:"transparent"

    property var show_text:qsTr("BACK")
    property var show_source:"img/button/7.png"


    Image{
        width:187
        height:91
        source:show_source
    }


    Text{
        x: 25
        y: 31
        font.family:"Microsoft YaHei"
        text:show_text
        color:"#ff0000"
        font.pixelSize:24
        anchors.centerIn: parent;
    }
}

