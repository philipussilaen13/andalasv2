import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width: 1024
    height: 768
    property int logo_y: 40
    property bool img_vis: false
    property var show_img: "img/background/1.jpg"
    property bool gif_vis: false
    property var show_gif: ""
    color: 'transparent'

    //Base background
    Image{
        id: img_showing
        visible: img_vis
        x:0
        y:0
        width:1024
        height:768
        source:show_img
    }

    AnimatedImage{
        id: gif_showing
        visible: gif_vis
        x:0
        y:0
        width:1024
        height:768
        source:show_gif
    }

    //Left Logo
   /* Image{
        x:0
        y:0
        width:1024
        height:768
        source:"img/background/2.png"
    }*/

    //Right Logo
    Image{
        id: rlogo_showing
        x:745
        y:logo_y
        width:235
        height:69
        source:"img/popbox-logo.png"
    }
}
