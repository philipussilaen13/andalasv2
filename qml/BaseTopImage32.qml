import QtQuick 2.0
import QtQuick.Controls 1.2

Rectangle{
    width: 1080
    height: 1920-768
    color: "black"
    property int timer_value: 10
    property bool isEmergency: root.isEmergency

    Stack.onStatusChanged:{
       if(Stack.status==Stack.Activating){
           abc.counter = timer_value
           my_timer.restart()
//           slot_handler.start_get_detection("face")
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        width:10
        height:10
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    if (isEmergency){
                        stack_view_top.push(emergency_view)
                    } else {
                        stack_view_top.push(ad_page_slider_32)
                        //stack_view_top.push(ad_page_32, {mode:"mediaPlayer", set_height: "normal"})
                    }
//                    stack_view_top.push(ad_page_32, {mode:"mediaPlayer", set_height: "full"})
//                    root.my_stack_view_visible = false
                }
            }
        }
    }

    Image{
            anchors.fill: parent
            source:"img/popbox_base.png"
            fillMode: Image.PreserveAspectFit
        }


}

