import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0

Background{

    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            slot_handler.start_get_product_file()
        }
        if(Stack.status==Stack.Deactivating){
            gridModel.clear()
            listmodel.clear()
            my_timer.stop()

        }
        if(Stack.status==Stack.Active){
        }
    }


    Rectangle{
        width:39
        height:11
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Text{
            id:countShow_test
            x:10
            y:10
            anchors.centerIn:parent
            color:"#FFFFFF"
            font.family:"Microsoft YaHei"
            font.pixelSize:24
        }


        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    Text {
        id: text1
        x: 0
        y: 110
        width: 1024
        height: 50
        text: qsTr("Please choose the product")
        font.family:"Microsoft YaHei"
        color:"#FFFFFF"
        textFormat: Text.PlainText
        font.pointSize: 30
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignBottom
    }

    Image {
        id: image1
        x: 66
        y: 166
        width: 900
        source: "img/courier19/stripe.png"
    }


    PickUpButton {
        id:pick_up_Button2
        x: 65
        y: 638
        show_text: qsTr("back")
        MouseArea {
            anchors.rightMargin: -4
            anchors.bottomMargin: -2
            anchors.leftMargin: 4
            anchors.topMargin: 2
            anchors.fill: parent
            onClicked:{
                my_stack_view.pop()
            }
            onEntered:{
                pick_up_Button2.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                pick_up_Button2.show_source = "img/button/7.png"
            }
        }
    }


    Item  {
        id: page
        x:66
        y:180
        width:1000
        height:430

        GridView{
            id:gridview
            anchors.fill: parent
            delegate: griddel
            model:gridModel
            cellWidth: 234
            cellHeight: height/2
            interactive: false
            contentX: listview.contentX

        }

        ListView {
            id: listview
            anchors.fill: parent
            model: listmodel
            delegate: delegate
            interactive: false
            orientation:ListView.Horizontal
            spacing:235
        }


        Component {
            id: griddel
            Rectangle{
                width: 220
                height: 220
                color: "transparent"
                Image {
                    id: image1
                    width: 200
                    height: 200
                    source: show_express_image

                    Text {
                        id: text1
                        x: 0
                        y: 0
                        width: 50
                        height: 50
                        text:show_text
                        visible: false
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 12
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            my_stack_view.push(web_view_page, {initurl: show_text})
                          //  my_stack_view.push(web_view_page)
                            abc.counter = timer_value
                            my_timer.stop()
                        }
                    }
                }
            }
        }

        ListModel {
            id: gridModel
        }

        Component{
            id:delegate
            Rectangle {
                width: 880
                height: 100
                color: "transparent"
            }
        }

        ListModel{
            id:listmodel
        }
    }


    Component.onCompleted:{
        root.product_file_result.connect(get_product)
    }

    Component.onDestruction:{
        root.product_file_result.disconnect(get_product)
    }

    function get_product(text){
        var obj = JSON.parse(text)
        for(var j=0;j<2;j++){
            for(var i in obj){
                var picname = obj[i].productname
                var url = obj[i].url
                gridModel.append({"show_text":url, "show_express_image":"img/product/"+picname});
                listmodel.append({});
            }
        }
    }
}
