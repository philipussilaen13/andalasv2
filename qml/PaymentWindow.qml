import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:payment_floating_window
    property bool e_logo_vis: false
    visible: false
    property bool useBaseOpacity: false
    property real globalOpacity: 0.9
    color: "transparent"
    width: 1024
    height: 768

    Rectangle{
        id: opacity_rec
        anchors.fill: parent
        color: "#828282"
        visible: useBaseOpacity
    }

    Rectangle{
        id: rectangle1
        x: 122
        y: 209
        width: 780
        height: 350
        color: "#ffffff"
        opacity: globalOpacity
        /*Image{
            id: reader
            x:50
            y:120
            width:244
            height:190
            source: "img/otherservice/icon_reader_portrait.png"
        }
        Image{
            id: emoney_logo
            visible: e_logo_vis
            x:164
            y:274
            width:130
            height:36
            source: "img/otherservice/logo-emoney300.png"
        }*/
        AnimatedImage{
            id: animated_emoney_use
            x: 0
            y: 50
            width: 300
            height: 300
            fillMode: Image.PreserveAspectFit
            source: "img/popsend/tapping_emoney_white.gif"
        }
    }
    function open(){
        payment_floating_window.visible = true
    }
    function close(){
        payment_floating_window.visible = false
    }
}
