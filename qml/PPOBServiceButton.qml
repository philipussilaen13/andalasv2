import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle
    property var show_text:""
    property var show_image:""
    property var show_text_color:"#ffffff"
    property var sim_operator: undefined
    width:245
    height:235
    radius: 20
    Image{
        anchors.fill: parent
        source:show_image
        fillMode: Image.PreserveAspectFit
        scale: 0.9
    }

    function get_logo(o){
        switch(o){
        case 'telkomsel':
            return "img/sepulsa/logo/telkomsel.png"
        case 'indosat':
            return "img/sepulsa/logo/indosat.png"
        case 'xl':
            return "img/sepulsa/logo/xl.png"
        case 'bolt':
            return "img/sepulsa/logo/bolt.png"
        default:
            return "img/sepulsa/logo/simpati.png"
        }
    }

    Image {
        visible: (sim_operator==undefined) ? false : true
        source: get_logo(sim_operator)
//        source: "img/sepulsa/logo/indosat.png"
        width: 150
        height: 80
        anchors.horizontalCenter: parent.horizontalCenter
        fillMode: Image.PreserveAspectFit
    }

    Text{
        text:show_text
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        font.family:"Microsoft YaHei"
        color:show_text_color
        font.pixelSize:25
    }
}
