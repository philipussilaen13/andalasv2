import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:camera_floating_window
    width: 576
    height: 432
    visible: false
    property url show_gif: "img/item/progress-circle.gif"

    Rectangle{
        anchors.fill: parent
        width: 576
        height: 432
        color: "#ffffff"
        opacity: 0.9
        AnimatedImage{
            id: animatedGif
            x: 146
            y: 74
            width: 288
            height: 216
            fillMode: Image.PreserveAspectFit
            source: show_gif
        }
        Text {
            id: text_notif_1
            x: 165
            y: 324
            text: qsTr("Please Wait...")
            font.family:"Microsoft YaHei"
            color:"#ff0000"
            font.pixelSize: 40
        }
    }

    function open(){
        camera_floating_window.visible = true
    }
    function close(){
        camera_floating_window.visible = false
    }
}
