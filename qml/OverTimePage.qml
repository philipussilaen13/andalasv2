import QtQuick 2.4
import QtQuick.Controls 1.2
import "door_price.js" as DOORS

Background{
    id:over_time

    property var payment:"0.00"
    property var press: "0"
    property int timer_value: 60
    property bool isPopDeposit: true
    property var expressData: undefined
//    color: "darkred"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('isPopDeposit : ', isPopDeposit)
            console.log('express_data : ', expressData)
            get_subnotif_text()
            over_time_button.show_source = "img/button/7.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = 30
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Component.onCompleted: {
        root.overdue_cost_result.connect(process_result)
        slot_handler.customer_get_overdue_cost()
    }

    Component.onDestruction: {
        root.overdue_cost_result.disconnect(process_result)
    }

    function process_result(cost){
        over_time.payment = cost
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
            }
    }

    FullWidthReminderText{
        x: 0
        y:459
        remind_text: (isPopDeposit==true) ? qsTr("Your Parcel Usage Has Expired") : qsTr("Your parcel has expired and returned to the sender.")
        remind_text_size:"35"
    }

    FullWidthReminderText{
        id: second_notif
        x: 0
        y:524
        remind_text_size:"30"
    }

    function get_subnotif_text(){
        if (!isPopDeposit){
            second_notif.remind_text =  qsTr("Please contact (021) 2902 2537 for further inquiry.")
            return
        } else {
            if (expressData == undefined){
                second_notif.remind_text = qsTr("Please extend from PopBox App or Contact Us at (021) 2902 2537")
                return
            } else {
                second_notif.remind_text = qsTr("You need to extend this parcel, Please extend to continue")
                return
            }
        }
    }

    /*
{"overdueTime": 1539853636000, "storeTime": 1539767236000, "logisticsCompany_id": "161e5ed1140f11e5bdbd0242ac110001", "validateCode": "2863MW",
"box_id": "402880835751659201576557d0891313", "expressType": "COURIER_STORE", "syncFlag": 1, "expressNumber": "PDSL2JYAK",
"id": "0b6d3266d1ec11e88a73509a4ccea7f5", "groupName": "POPDEPOSIT", "version": 0, "mouth_id": "4028808359ff616b015a1728421f574b", "status": "IN_STORE",
"operator_id": "145b2728140f11e5bdbd0242ac110001", "takeUserPhoneNumber": "085710157057", "storeUser_id": "402880825dbcd4c3015de54d98c5518e"}
*/

    Image {
        id: img_parcel_return
        x: 374
        y: 126
        width: 275
        height: 275
        source: (isPopDeposit==true) ? "img/otherImages/parcel_expired.png" : "img/otherImages/parcel_returned.png"
    }

    OverTimeButton{
        id:over_time_button
        visible: (expressData!=undefined) ? true : false
        x:372
        y:616
        show_text:qsTr("Extend")
        show_x:0
        show_image:"img/05/back_red.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                var e = JSON.parse(expressData)
                var customers = JSON.stringify({ "name": e.takeUserPhoneNumber, "phone": e.takeUserPhoneNumber, "email": e.takeUserPhoneNumber})
                var products = JSON.stringify({"pin": e.validateCode, "id": e.id, "sku": e.expressNumber, "name": "PopSafe Locker Extend|" + e.lockerNo+"-"+e.lockerSize,
                                                  "qty": "1", "total_amount": DOORS.default_price, "lockerSize": e.lockerSize, "lockerNo": e.lockerNo})
                my_stack_view.push(grocery_express_info, {customers: customers, products: products, usageOf: "popsafe-extend", selected_locker: e.lockerName})
            }
            onEntered:{
                over_time_button.show_source = "img/bottondown/down1.png"
            }
            onExited:{
                over_time_button.show_source = "img/button/7.png"
            }
        }
    }
}
