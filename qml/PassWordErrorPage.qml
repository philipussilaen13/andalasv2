import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: background
    width: 1024
    height: 768

    property int timer_value: 60

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
            }
    }

    FullWidthReminderText{
        id:main_text
        x: 0
        y:450
        height: 100
        remind_text:qsTr("The code is not correct, please re-input")
        remind_text_size:"40"
    }

    Image {
        id: img_error_password
        x: 412
        y: 141
        width: 250
        height: 250
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/otherImages/error_notif.png"
    }

    OverTimeButton1{
        id:main_error_button
        x:7
        y:598
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 80
        anchors.horizontalCenter: parent.horizontalCenter
        show_text:qsTr("MAIN MENU")
        show_x:28
        show_image:"img/07/rewrite.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(null)
            }
            onEntered:{
                main_error_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                main_error_button.show_source = "img/05/button.png"
            }
        }
    }

    OverTimeButton{
        id:pass_error_button
        visible: false
        x:232
        y:598
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                pass_error_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                pass_error_button.show_source = "img/05/button.png"
            }
        }
    }
}
