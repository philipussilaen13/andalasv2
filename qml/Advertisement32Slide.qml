import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_root
    color: "transparent"
    property var img_path: "../advertisement/source/"
    property variant qml_pic: ["09_popbox-adalah.png"]
    property string pic_source: ""
    property int num_pic: 0
    property var locker_group: ["Indofood Tower", "Wisma Indomobil", "Wisma Indocement"]
    property bool isIndofood: false
    property bool emoney_reader: false

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_locker_name()
            //init_ad_images()
        }
        if(Stack.status==Stack.Deactivating){
            slider_timer.stop()
        }
    }

    Component.onCompleted: {
        slot_handler.start_get_locker_name();
        root.start_get_cod_status_result.connect(cod_result);
        root.start_get_locker_name_result.connect(define_indofood);
        //init_ad_images()
    }

    Component.onDestruction: {
        root.start_get_locker_name_result.disconnect(define_indofood);
        root.start_get_cod_status_result.disconnect(cod_result);
        slider_timer.stop();
    }

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result=="enabled"){
            emoney_reader = true;
        }
    }

    function define_indofood(text){
        if((text!=""||text!= undefined) && locker_group.indexOf(text) > -1){
            isIndofood = true;
        }
        init_ad_images()
    }


    function init_ad_images(){
        qml_pic = ["01_popsafe.png", "02_popsendv2.png", "04_khawatir-barang-rusak.png", "04_terima-paket-gak-pake-lama.png",
        "05_sibuk-kerja.png", "08_nunggu-kurir-lama.png"]

        if(isIndofood){
            if (emoney_reader){
                qml_pic = ["01_popsafe.png", "02_popsendv2.png", "04_khawatir-barang-rusak.png","04_terima-paket-gak-pake-lama.png",
                "05_sibuk-kerja.png", "08_nunggu-kurir-lama.png", "12_bisa-isi-pulsa-disini.png"]
            } else {
                qml_pic = ["01_popsafe.png", "02_popsendv2.png", "04_khawatir-barang-rusak.png","04_terima-paket-gak-pake-lama.png",
                "05_sibuk-kerja.png", "08_nunggu-kurir-lama.png"]
            }
        }

        loader.sourceComponent = loader.Null
        pic_source = img_path + qml_pic[0]
        loader.sourceComponent = component
        slider_timer.start()
    }


    Timer{
        id:slider_timer
        interval:10000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1
                if(num_pic == qml_pic.length){
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[0]
                    loader.sourceComponent = component
                    num_pic = 0
                }else{
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[num_pic]
                    loader.sourceComponent = component
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                x: 0
                y: 0
                width: parent_root.width
                height: parent_root.height
                source: pic_source
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Loader { id: loader }

}

