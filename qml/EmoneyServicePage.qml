import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:emoney_service_page
    property int timer_value: 60
    property var press: "0"
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


//    Rectangle{
//        anchors.fill: parent
//        color: 'darkred'
//    }

    FullWidthReminderText{
        id:page_title
        y:148
        remind_text:qsTr("Please select option")
        remind_text_size:"45"
    }

    Row{
        id: buttons_group
        y: 250
        width: 1000
        height: 250
        anchors.horizontalCenter: parent.horizontalCenter

        OtherServiceButton{
            id:payOrder_button
            width: 245
            height: 235
            scale: 0.8
            show_image:"img/button/payOrder_white.png"
            show_text:qsTr("Pay Order")
            show_text_color:"#ab312e"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_timer.stop()
                    my_stack_view.push(other_input_memory_page)
                }
                onEntered:{
                    payOrder_button.show_image = "img/button/payOrder_red.png"
                }
                onExited:{
                    payOrder_button.show_image = "img/button/payOrder_white.png"
                }
            }
        }

        OtherServiceButton{
            id:checkBalance_button
            width: 245
            height: 235
            scale: 0.8
            show_image:"img/button/balanceCheck_white.png"
            show_text:qsTr("Check Balance")
            show_text_color:"#ab312e"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_timer.stop()
                    my_stack_view.push(emoney_check_balance)
                }
                onEntered:{
                    checkBalance_button.show_image = "img/button/balanceCheck_red.png"
                }
                onExited:{
                    checkBalance_button.show_image = "img/button/balanceCheck_white.png"
                }
            }
        }

        OtherServiceButton {
            id: sepulsa_button
            width: 245
            height: 235
            scale: 0.8
            show_text_color: "#ab312e"
            show_text: qsTr("Phone Credit")
            show_image: "img/button/sepulsa_white.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_timer.stop()
                    my_stack_view.push(sepulsa_input_phone_view)
                }
                onEntered:{
                    sepulsa_button.show_image = "img/button/sepulsa_red.png"
                }
                onExited:{
                    sepulsa_button.show_image = "img/button/sepulsa_white.png"
                }
            }
        }

        OtherServiceButton {
            id: topup_popsend_button
            width: 245
            height: 235
            scale: 0.8
            show_text_color: "#ab312e"
            show_text: qsTr("Top Up PopSend")
            show_image: "img/popsend/topup_popsend_white.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    my_timer.stop()
                    my_stack_view.push(courier_input_phone_view, {usageOf:"popsend_topup"})
                }
                onEntered:{
                    topup_popsend_button.show_image = "img/popsend/topup_popsend_red.png"
                }
                onExited:{
                    topup_popsend_button.show_image = "img/popsend/topup_popsend_white.png"
                }
            }
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {
    }

    Text {
        id: powered_text
        x: 228
        y: 638
        width: 250
        height: 40
        color: "#ffffff"
        text: qsTr("Powered by :")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 30
        font.family: "Microsoft YaHei"
    }

    Rectangle {
        id: rec_emoney
        x: 488
        y: 619
        width: 300
        height: 77
        color: "#ffffff"

        Image {
            id: image_emoney
            x: 0
            y: 0
            width: 300
            height: 77
            source: "img/otherservice/logo-emoney300.png"
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                back_button.show_source = "img/button/7.png"
            }
        }
    }
}
