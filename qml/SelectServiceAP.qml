import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    mainMode: true
    property var press: '0'
    property variant pic_source: []
    property variant qml_pic: []
    property var path: '/advertisement/apservice/'
    property var img_path: '..' + path
    property var doorSize: 'M'
    property bool availableUse: false

    property int num_pic: 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_file_dir(path);
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_idle_mode();
            timer_clock.start();
            press = '0';
            availableUse = false;
        }
        if(Stack.status==Stack.Deactivating){
            timer_clock.stop();
            slider_timer.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_file_dir_result.connect(init_images);
        root.free_mouth_result.connect(show_free_mouth_num);
    }

    Component.onDestruction: {
        root.start_get_file_dir_result.disconnect(init_images);
        root.free_mouth_result.disconnect(show_free_mouth_num);
    }

    function show_free_mouth_num(doors){
        var obj = JSON.parse(doors);
        for(var i in obj){
            if(i == doorSize){
                console.log('available compartment', doorSize, ':', obj[i]);
                if (obj[i] > 0) {
                    availableUse = true;
                    console.log('ready to use :', availableUse);
                }
            }
        }
    }

    function init_images(result){
//        console.log(result);
        if (result=='') return;
        var i = JSON.parse(result);
        qml_pic = i.output;
        slider.sourceComponent = slider.Null;
        pic_source = img_path + qml_pic[0];
        slider.sourceComponent = component;
        slider_timer.start();
        loading_image.visible = false;
    }


    Timer{
        id:slider_timer
        interval:10000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1;
                if(num_pic == qml_pic.length){
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[0];
                    slider.sourceComponent = component;
                    num_pic = 0;
                }else{
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[num_pic];
                    slider.sourceComponent = component;
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                source: pic_source
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
            }
        }
    }

    Loader {
        id: slider
        x: 0
        y: 90
        width: parent.width
        height: 490
    }

    AnimatedImage{
        id: loading_image
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'img/apservice/loading.gif'
        fillMode: Image.PreserveAspectFit
    }

    MainButtonAP{
        id: titip
        color: "#009BE1"
        main_text: qsTr("TITIP")
        desc_text: qsTr("Titip Barang Anda di Loker")
        button_img: "img/apservice/icon/titip.png"
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (availableUse){
                    if (press!='0') return;
                    press = '1';
                    //console.log('TITIP Button is Pressed');
                    my_stack_view.push(input_phone_ap);
                    slot_handler.stop_idle_mode();
                } else {
                    notif_text.open();
                }
            }
        }
    }

    MainButtonAP{
        id: ambil
        color: "#65B82E"
        main_text: qsTr("AMBIL")
        desc_text: qsTr("Ambil Barang Anda dari Loker")
        button_img: "img/apservice/icon/ambil.png"
        anchors.right: parent.right
        anchors.rightMargin: 50
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return
                press = '1';
                //console.log('AMBIL Button is Pressed');
                my_stack_view.push(input_pin_ap);
                slot_handler.stop_idle_mode();
            }
        }
    }

    Text {
        id: timeText
        width: 150
        height: 35
        text: new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
        font.bold: true
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 20
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:25
        color:"#ffffff"
    }

    Text {
        id: dateText
        height: 25
        text: new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat)
        anchors.top: parent.top
        anchors.topMargin: 51
        anchors.right: parent.right
        anchors.rightMargin: 20
        font.italic: false
        verticalAlignment: Text.AlignVCenter
        font.family:"Microsoft YaHei"
        font.pixelSize:15
        color:"#ffffff"
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:
        {
            timeText.text = new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss");
            press = '0';
        }
    }


    Notification{
        id: notif_text
        contentNotif: qsTr('Mohon Maaf, Tidak ada loker tersedia saat ini.')
        successMode: false
        redImage: true
    }


}
