import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:laundry_info
    property int timer_value: 30

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/error_down.png"
            }
            onExited:{
                back_button.show_source = "img/05/button.png"
            }
        }
    }

    Image{
        id:next_button
        x: 390
        y: 627
        width:250
        height:60
        source:"img/button/1.png"
        Text{
            text:qsTr("Drop My Laundry")
            font.bold: false
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"red"
            font.pixelSize:28
            anchors.centerIn: parent;
        }
        MouseArea {
            width: 235
            height: 60
            enabled: true
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(laundry_input_memory_page)
            }
            onEntered:{
                next_button.source = "img/button/DeleteButton.png"
            }
            onExited:{
                next_button.source = "img/button/1.png"
            }
        }
    }


    GroupBox {
        id: groupStepImages
        x: 0
        y: 135
        width: 1023
        height: 380
        checkable: false
        flat: true
        title: qsTr("")
        checked: false

        Image {
            id: step1
            width: 350
            height: 375
            scale: 0.9
            fillMode: Image.PreserveAspectFit
            source: "img/laundry/info_1.png"
        }

        Image {
            id: step2
            x: 327
            width: 350
            height: 375
            scale: 0.9
            fillMode: Image.PreserveAspectFit
            source: "img/laundry/info_2.png"
        }

        Image {
            id: step3
            x: 658
            width: 350
            height: 375
            scale: 0.9
            fillMode: Image.PreserveAspectFit
            source: "img/laundry/info_3.png"
        }
    }

    GroupBox {
        id: groupStepTexts
        x: 0
        y: 495
        width: 1023
        height: 120
        checkable: false
        flat: true
        title: qsTr("")
        checked: false

        Text {
            id: textstep1
            x: 28
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Order your laundry from merchant web/apps, Ensure the laundry confirmation")
            textFormat: Text.AutoText
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 20
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }

        Text {
            id: textstep2
            x: 365
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Go to Locker, Scan/Enter the laundry number and choose suitable door size (M or L).")
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 20
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }

        Text {
            id: textstep3
            x: 696
            width: 276
            height: 125
            color: "#ffffff"
            text: qsTr("Drop off your laundry in opened door, Then Courier will pick it up soon.")
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 20
            font.italic: true
            font.family:"Microsoft YaHei"
            wrapMode: Text.WordWrap
        }
    }

    Text {
        id: supported_by_text
        visible: false
        x: 20
        y: 635
        text: qsTr("Supported by :")
        style: Text.Normal
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        width: 268
        height: 50
        color: "#ffffff"
        font.pixelSize: 25
        font.italic: true
        font.family:"Microsoft YaHei"
    }

    GroupBox {
        id: groupLogoImages
        x: 50
        y: 680
        width: 950
        height: 80
        flat: true
        title: qsTr("")
        checked: false
        checkable: false
        visible: false

        Image {
            id: omaisu_logo
            x: 680
            y: 0
            width: 250
            height: 70
            source: "img/laundry/omaisu_logo.png"
            fillMode: Image.PreserveAspectFit
        }

        Image {
            id: ttp_logo
            x: 478
            width: 250
            height: 70
            source: "img/laundry/taptopick_logo.png"
            fillMode: Image.PreserveAspectFit
        }

        Image {
            id: tayaka_logo
            x: 256
            width: 250
            height: 70
            source: "img/laundry/tayaka_logo.png"
            fillMode: Image.PreserveAspectFit
        }

        Image {
            id: knk_logo
            x: 7
            width: 250
            height: 70
            fillMode: Image.PreserveAspectFit
            source: "img/laundry/knk_logo.png"
        }
    }
}
