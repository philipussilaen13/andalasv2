import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: background1
    width: 1024
    height: 768
    property int timer_value: 60
    property var press: "0"
    property var c_access: "full"
    property var fromExpress: "No"
    property var pref_login_user: "undefined"
    property var courier_name: "undefined"
    property var courier_com: "undefined"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('courier_credential : ' + c_access + ', with prefix_user_account : ' + pref_login_user)
            slot_handler.get_user_info()
            slot_handler.start_load_courier_overdue_express_count()
            courier_take_package_button.show_source = "img/button/1.png"
            courier_take_overdue_button.show_source = "img/button/1.png"
            courier_take_it_button.show_source = "img/button/1.png"
            history_button.show_source = "img/button/1.png"
            other_services_button.show_source = "img/button/1.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
            if(c_access=="limited"){
                courier_take_reject_button.visible = false
                courier_take_reject_button.enabled = false
                courier_take_package_button.x = 215
                courier_take_overdue_button.x = 595
                courier_take_it_button.x = 405
            }
            if(fromExpress=="Yes"){
                courier_take_reject_button.enabled = false
                courier_take_overdue_button.enabled = false
                history_button.enabled = false
                other_services_button.enabled = false
                courier_take_package_button.x = 531
                courier_take_it_button.x = 301
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
        root.user_info_result.connect(detail_user)
        root.overdue_express_count_result.connect(overdue_count)
    }

    Component.onDestruction: {
        root.user_info_result.disconnect(detail_user)
        root.overdue_express_count_result.disconnect(overdue_count)
    }

    function overdue_count(text){
        overdue_num.text = text
    }

    function detail_user(text){
        var result = JSON.parse(text)
        courier_name = result.name
        courier_com = result.company_name
        if(result.company_name.indexOf("GRAB") > -1){
            courier_com = "GRAB Indonesia"
        }else if(result.company_name.indexOf("Merchant") > -1){
            courier_take_reject_button.visible = false
            courier_take_it_button.visible = false
            courier_take_package_button.x = 328
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Text {
        id: greeting_text
        x: 37
        y: 115
        width: 200
        height: 40
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        text: 'Hi, ' + courier_name + ' | ' + courier_com
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:22
        font.capitalization: Font.Capitalize
    }


    FullWidthReminderText{
        y:170
        remind_text:qsTr("Please select service options")
        remind_text_size:"35"
    }

    BackButton{
        id:select_service_back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_package_button
        x:138
        y:255
        show_text:qsTr("Store")
        show_image:"img/courier10/StorageArticle1.png"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(courier_memory_view, {c_access:c_access, pref_login_user:pref_login_user})
            }
            onEntered:{
                courier_take_package_button.show_image = "img/bottondown/button_1.png"
                courier_take_package_button.show_text_color = "white"
            }
            onExited:{
                courier_take_package_button.show_image = "img/courier10/StorageArticle1.png"
                courier_take_package_button.show_text_color = "#BF2E26"
                courier_take_package_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_overdue_button
        x:517
        y:255
        show_text:qsTr("Overdue")
        show_image:"img/courier10/overduearticle1.png"
        show_source:"img/button/1.png"
        visible: (fromExpress=="Yes") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(background_overdue_time_view)
            }
            onEntered:{
                courier_take_overdue_button.show_image = "img/bottondown/button_2.png"
                courier_take_overdue_button.show_text_color = "white"
            }
            onExited:{
                courier_take_overdue_button.show_image = "img/courier10/overduearticle1.png"
                courier_take_overdue_button.show_text_color = "#BF2E26"
                courier_take_overdue_button.show_source = "img/button/1.png"
            }
            Text {
                id: overdue_num
                x: 127
                y: 0
                color: "#BF2E26"
                text: qsTr("10")
                anchors.right: parent.right
                anchors.rightMargin: 0
                styleColor: "#BF2E26"
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                font.bold: true
                font.pixelSize: 24
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_it_button
        x:328
        y:255
        show_text:qsTr("Take")
        show_image:"img/courier10/SendArticle1.png"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(take_send_express_page)
            }
            onEntered:{
                courier_take_it_button.show_image = "img/bottondown/button_3.png"
                courier_take_it_button.show_text_color = "white"
            }
            onExited:{
                courier_take_it_button.show_image = "img/courier10/SendArticle1.png"
                courier_take_it_button.show_text_color = "#BF2E26"
                courier_take_it_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceUpButton{
        id:courier_take_reject_button
        x:707
        y:255
        show_text:qsTr("Reject")
        show_image:"img/courier10/takereject.png"
        show_source:"img/button/1.png"
        visible: (fromExpress=="Yes" || c_access=="limited") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(take_reject_express)
            }
            onEntered:{
                courier_take_reject_button.show_image = "img/bottondown/button_6.png"
                courier_take_reject_button.show_text_color = "white"
            }
            onExited:{
                courier_take_reject_button.show_image = "img/courier10/takereject.png"
                courier_take_reject_button.show_text_color = "#BF2E26"
                courier_take_reject_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceDownButton{
        id:other_services_button
        x:517
        y:603
        show_text:qsTr("Other services")
        show_image:"img/courier10/otherservices1.png"
        show_source:"img/button/1.png"
        visible: (fromExpress=="Yes") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                other_services_button.show_image = "img/bottondown/button_5.png"
                other_services_button.show_text_color = "white"
            }
            onExited:{
                other_services_button.show_image = "img/courier10/otherservices1.png"
                other_services_button.show_text_color = "#BF2E26"
                other_services_button.show_source = "img/button/1.png"
            }
        }
    }

    CourierServiceDownButton{
        id:history_button
        x:257
        y:603
        show_text:qsTr("History")
        show_image:"img/courier10/History1.png"
        show_source:"img/button/1.png"
        visible: (fromExpress=="Yes") ? false : true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(on_develop_view)
            }
            onEntered:{
                history_button.show_image = "img/bottondown/button_4.png"
                history_button.show_text_color = "white"
            }
            onExited:{
                history_button.show_image = "img/courier10/History1.png"
                history_button.show_text_color = "#BF2E26"
                history_button.show_source = "img/button/1.png"
            }
        }
    }

    Item{
        id: how_to_use_info
        x: 105
        y:525
        width: 900
        height: 125
        anchors.horizontalCenter: parent.horizontalCenter
        visible: (fromExpress=="Yes") ? true : false
        property int item_font_size: 17
        property real opacity_rec: 0.4

        Rectangle{
            id: col_howto_1
            x:0
            color: "black"
            opacity: how_to_use_info.opacity_rec
            radius: 10
            height: parent.height
            width: (parent.width/2) - 5
        }
        Column{
            anchors.fill: col_howto_1
            spacing: 5
            Text{
                id: how_to_a1
                width: parent.width
                text: qsTr("TAKE :")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:22
            }
            Text{
                id: how_to_a2
                width: parent.width
                text: qsTr("Select this button to collect parcel/laundry from this locker.")
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:how_to_use_info.item_font_size
                font.italic: true
            }
        }

        Rectangle{
            id: col_howto_2
            color: "black"
            opacity: how_to_use_info.opacity_rec
            radius: 10
            x:(parent.width/2) + 10
            height: parent.height
            width: (parent.width/2) - 5
        }
        Column{
            anchors.fill: col_howto_2
            spacing: 5
            Text{
                id: how_to_b1
                width: parent.width
                text: qsTr("STORE :")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:22
            }
            Text{
                id: how_to_b2
                width: parent.width
                text: qsTr("Select this button to store parcels into this locker.")
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family:"Microsoft YaHei"
                color:"white"
                textFormat: Text.PlainText
                font.pointSize:how_to_use_info.item_font_size
                font.italic: true
            }
        }
    }

    LoadingView{
        id: loadingGif
    }
}
