import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{

    width:180
    height:200
    color:"transparent"

    property var show_text:""
    property var show_text_color:"#BF2E26"
    property var show_image:""
    property var show_source:"img/button/1.png"


    Image{
        x:0
        y:0
        width:180
        height:200
        source:show_source
    }


    Image{
        x:0
        y:0
        width:180
        height:200
        source:show_image
    }


    Rectangle{

        y:150
        width:180
        height:30
        color:"transparent"


        Text{
            text:show_text
            font.family:"Microsoft YaHei"
            color:show_text_color
            font.pixelSize:28
            anchors.centerIn: parent;
        }
    }


}
