import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property var press:"0"
    property int timer_value: 300

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_version()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Rectangle{
        id:main_page
        Component.onCompleted: {
            root.init_client_result.connect(handle_result)
            root.get_version_result.connect(show_version)
            //root.open_main_box_result.connect(open_mouth)
            root.box_start_migrate_result.connect(box_migrate)
        }

        Component.onDestruction: {
            root.init_client_result.disconnect(handle_result)
            root.get_version_result.disconnect(show_version)
            //root.open_main_box_result.disconnect(open_mouth)
            root.box_start_migrate_result.disconnect(box_migrate)
        }

    function box_migrate(result){
        press = "0"
        waiting.close()
        main_page.enabled = false
        img_init_ok.source = "img/otherImages/x-mark.png"
        if (result == ''){
            text_init_ok.text = qsTr("Please Init DB First..!")
        } else if (result == 'ERROR'){
            text_init_ok.text = qsTr("Please Ensure Init DB is correct!")
        } else if (result == 'FAILED') {
            text_init_ok.text = qsTr("Something went wrong..!")
        } else if (result == 'SUCCESS') {
            img_init_ok.source = "img/otherImages/checklist.png"
            text_init_ok.text = qsTr("Data Migration is Success")
        }
        init_ok.open()
    }


    function show_version(text){
           version.text = text
        }

    function handle_result(text){
        press = "0"
        waiting.close()
        if(text == "Success"){
            main_page.enabled = false
            init_ok.open()
        }
    }

    function open_mouth(result){
        press = "0"
        waiting.close()
        if(result == 'Success'){
            open_mouth_success.open()
        }
        if(result == 'Failure'){
            open_mouth_failure.open()
        }
    }

    BackButton{
        x:584
        y:630
        show_text:qsTr("Back")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.pop()
            }
        }
    }

    ManagerServiceButton1{
        x:272
        y:464
        show_text:qsTr("Init DB")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                slot_handler.start_init_client()
                main_page.enabled = false
                waiting.open()
            }
        }
    }

    ManagerServiceButton1{
        x:522
        y:464
        show_text:qsTr("Migrate DB")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                slot_handler.box_start_migrate()
                main_page.enabled = false
                waiting.open()
            }
        }
    }
}


HideWindow{
    id:waiting
    //visible: true

    Image {
        id: img_time_waiting
        x: 437
        y: 362
        width: 150
        height: 200
        source: "img/otherImages/loading.png"
    }

    Text {
        y:266
        width: 1024
        height: 60
        text: qsTr("Please wait")
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        textFormat: Text.PlainText
        font.pointSize:45
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignBottom
    }
}

    HideWindow{
        id:init_ok
        //visible: true

        Image {
            id: img_init_ok
            x: 413
            y: 218
            width: 200
            height: 200
            fillMode: Image.PreserveAspectFit
            source: "img/otherImages/checklist.png"
        }

        Text {
            text: qsTr("Successful")
            id: text_init_ok
            x: 94
            y:464
            width: 852
            height: 80
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:init_ok_back
            x:374
            y:590
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    main_page.enabled = true
                    init_ok.close()
                }
                onEntered:{
                    init_ok_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    init_ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    Row {
        x: 8
        y: 750
        spacing: 8

        Text {
            text: qsTr("Version:")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 12
        }

        Text {
            id: version
            text: qsTr("Text")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            font.pixelSize: 12
        }
    }

    HideWindow{
        id:open_mouth_success
        //visible: true

        Image {
            id: img_open_mouth_success
            x: 413
            y: 218
            width: 200
            height: 200
            source: "img/otherImages/checklist.png"
        }

        Text {
            text: qsTr("open mouth success")
            x: 94
            y:464
            width: 852
            height: 80
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:open_mouth_success_back
            x:374
            y:590
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    open_mouth_success.close()
                }
                onEntered:{
                    open_mouth_success_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    open_mouth_success_back.show_source = "img/button/7.png"
                }
            }
        }
    }

    HideWindow{
        id:open_mouth_failure
        //visible: true

        Image {
            id: img_open_mouth_failure
            x: 413
            y: 218
            width: 200
            height: 200
            source: "img/otherImages/x-mark.png"
        }

        Text {
            text: qsTr("open mouth failure")
            x: 94
            y:464
            width: 852
            height: 80
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        OverTimeButton{
            id:open_mouth_failure_back
            x:374
            y:590
            show_text:qsTr("back")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    abc.counter = timer_value
                    my_timer.restart()
                    main_page.enabled = true
                    open_mouth_failure.close()
                }
                onEntered:{
                    open_mouth_failure_back.show_source = "img/bottondown/down_1.png"
                }
                onExited:{
                    open_mouth_failure_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
