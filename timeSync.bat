@echo off
:: BatchGotAdmin (Run as Admin code starts)
REM --> Check for permissions
	>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
REM --> If error flag set, we do not have admin.

if '%errorlevel%' NEQ '0' (
	echo Requesting administrative privileges...
	goto UACPrompt
) else ( 
	goto gotAdmin 
)

:UACPrompt
	echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
	echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
	"%temp%\getadmin.vbs"
exit /B

:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
	pushd "%CD%"
	CD /D "%~dp0"
:: BatchGotAdmin (Run as Admin code ends)

@echo on & @setlocal enableextensions
::@echo =========================
::@echo Turn off the time service
net stop w32time
::@echo ======================================================================
::@echo Set the SNTP (Simple Network Time Protocol) source for the time server
w32tm /config /syncfromflags:manual /manualpeerlist:"0.it.pool.ntp.org 1.it.pool.ntp.org 2.it.pool.ntp.org 3.it.pool.ntp.org"
::@echo =============================================
::@echo ... and then turn on the time service back on
net start w32time
::@echo =============================================
::@echo Tell the time sync service to use the changes
w32tm /config /update
::@echo =======================================================
@echo Time Sync is Success
w32tm /resync /rediscover
@endlocal & @goto :EOF