import os
import sys
from PyQt5.QtCore import QUrl, QObject, pyqtSlot, QTranslator, Qt
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQuick import QQuickView
import pygame
import wmi
import Configurator
import device
from box.service import BoxService
from database import ClientDatabase
from device import Camera
from express.service import ExpressService
from express.popbox import PopboxService
from network import HttpClient
from sync import PushMessage, PullMessage
from user.service import UserService
from alert.service import AlertService
from service import PPOB
from service import APService
import logging
import logging.handlers
import subprocess
import atexit
import json
from time import *
import ClientTools
import winreg



class SlotHandler(QObject):
    __qualname__ = 'SlotHandler'

    def select_language(self, string):
        translator.load(path + string)

    select_language = pyqtSlot(str)(select_language)

    def start_get_version(self):
        BoxService.start_get_version()

    start_get_version = pyqtSlot()(start_get_version)

    def clean_user_token(self):
        HttpClient.clean_user_token()

    clean_user_token = pyqtSlot()(clean_user_token)

    def start_update_system(self):
        os.system('update.bat')

    start_update_system = pyqtSlot()(start_update_system)

    def customer_take_express(self, string):
        ExpressService.start_customer_take_express(string)

    customer_take_express = pyqtSlot(str)(customer_take_express)

    def customer_scan_barcode_take_express(self):
        ExpressService.start_barcode_take_express()

    customer_scan_barcode_take_express = pyqtSlot()(customer_scan_barcode_take_express)

    def customer_cancel_scan_barcode(self):
        ExpressService.stop_barcode_take_express()

    customer_cancel_scan_barcode = pyqtSlot()(customer_cancel_scan_barcode)

    def get_express_mouth_number(self):
        BoxService.get_express_mouth_number()

    get_express_mouth_number = pyqtSlot()(get_express_mouth_number)

    # def start_get_ups_status(self):
    #     BoxService.start_get_ups_status()
    #
    # start_get_ups_status = pyqtSlot()(start_get_ups_status)

    def customer_get_overdue_cost(self):
        ExpressService.get_overdue_cost()

    customer_get_overdue_cost = pyqtSlot()(customer_get_overdue_cost)

    def background_login(self, username, password, identity):
        UserService.background_login_start(username, password, identity)

    background_login = pyqtSlot(str, str, str)(background_login)

    def start_courier_scan_barcode(self):
        ExpressService.start_get_express_number_by_barcode()

    start_courier_scan_barcode = pyqtSlot()(start_courier_scan_barcode)

    def stop_courier_scan_barcode(self):
        ExpressService.stop_get_express_number_by_barcode()

    stop_courier_scan_barcode = pyqtSlot()(stop_courier_scan_barcode)

    def set_express_number(self, express_number):
        ExpressService.set_express_number(express_number)

    set_express_number = pyqtSlot(str)(set_express_number)

    def set_phone_number(self, phone_number):
        ExpressService.set_phone_number(phone_number)

    set_phone_number = pyqtSlot(str)(set_phone_number)

    def start_store_express(self):
        ExpressService.start_store_express()

    start_store_express = pyqtSlot()(start_store_express)

    # def start_pay_cash_for_overdue_express(self):
    #     ExpressService.start_pay_cash_for_overdue_express()

    # start_pay_cash_for_overdue_express = pyqtSlot()(start_pay_cash_for_overdue_express)

    def get_user_info(self):
        UserService.get_user_info()

    get_user_info = pyqtSlot()(get_user_info)

    def manager_get_box_info(self):
        BoxService.start_manager_get_box_info()

    manager_get_box_info = pyqtSlot()(manager_get_box_info)

    def start_get_customer_store_express_info(self, customer_store_number):
        ExpressService.start_get_customer_store_express_info(customer_store_number)

    start_get_customer_store_express_info = pyqtSlot(str)(start_get_customer_store_express_info)

    def start_get_customer_reject_express_info(self, customer_reject_number):
        ExpressService.start_get_customer_reject_express_info(customer_reject_number)

    start_get_customer_reject_express_info = pyqtSlot(str)(start_get_customer_reject_express_info)

    def start_customer_scan_qr_code(self):
        PopboxService.start_customer_scan_qr_code()

    start_customer_scan_qr_code = pyqtSlot()(start_customer_scan_qr_code)

    def stop_customer_scan_qr_code(self):
        PopboxService.stop_customer_scan_qr_code()

    stop_customer_scan_qr_code = pyqtSlot()(stop_customer_scan_qr_code)

    def start_get_mouth_status(self):
        BoxService.start_get_mouth_status()

    start_get_mouth_status = pyqtSlot()(start_get_mouth_status)

    def start_get_free_mouth_mun(self):
        BoxService.start_get_free_mouth_mun()

    start_get_free_mouth_mun = pyqtSlot()(start_get_free_mouth_mun)

    def start_courier_load_overdue_express_list(self, page):
        ExpressService.start_courier_load_overdue_express_list(page)

    start_courier_load_overdue_express_list = pyqtSlot(int)(start_courier_load_overdue_express_list)

    def start_manager_load_overdue_express_list(self, page):
        ExpressService.start_manager_load_overdue_express_list(page)

    start_manager_load_overdue_express_list = pyqtSlot(int)(start_manager_load_overdue_express_list)

    def start_load_courier_overdue_express_count(self):
        ExpressService.start_load_courier_overdue_express_count()

    start_load_courier_overdue_express_count = pyqtSlot()(start_load_courier_overdue_express_count)

    def start_load_manager_overdue_express_count(self):
        ExpressService.start_load_manager_overdue_express_count()

    start_load_manager_overdue_express_count = pyqtSlot()(start_load_manager_overdue_express_count)

    def start_open_mouth_again(self):
        BoxService.start_open_mouth()

    start_open_mouth_again = pyqtSlot()(start_open_mouth_again)

    def start_init_client(self):
        BoxService.start_init_client()

    start_init_client = pyqtSlot()(start_init_client)

    def start_courier_take_overdue_express(self, id_list):
        ExpressService.start_staff_take_overdue_express_list(id_list)

    start_courier_take_overdue_express = pyqtSlot(str)(start_courier_take_overdue_express)

    def start_courier_take_overdue_all(self):
        ExpressService.start_staff_take_all_overdue_express()

    start_courier_take_overdue_all = pyqtSlot()(start_courier_take_overdue_all)

    # def start_calculate_customer_express_cost(self):
    #     ExpressService.start_calculate_customer_express_cost()

    # start_calculate_customer_express_cost = pyqtSlot()(start_calculate_customer_express_cost)
    #
    # def start_calculate_customer_reject_express_cost(self):
    #     ExpressService.start_calculate_customer_reject_express_cost()

    # start_calculate_customer_reject_express_cost = pyqtSlot()(start_calculate_customer_reject_express_cost)

    def stop_calculate_customer_express_cost(self):
        ExpressService.stop_calculate_customer_express_cost()

    stop_calculate_customer_express_cost = pyqtSlot()(stop_calculate_customer_express_cost)

    def stop_calculate_customer_reject_express_cost(self):
        ExpressService.stop_calculate_customer_reject_express_cost()

    stop_calculate_customer_reject_express_cost = pyqtSlot()(stop_calculate_customer_reject_express_cost)

    # def start_pay_cash_for_customer_express(self):
    #     ExpressService.start_pay_cash_for_customer_express()
    #
    # start_pay_cash_for_customer_express = pyqtSlot()(start_pay_cash_for_customer_express)
    #
    # def start_pay_cash_for_customer_reject_express(self):
    #     ExpressService.start_pay_cash_for_customer_reject_express()
    #
    # start_pay_cash_for_customer_reject_express = pyqtSlot()(start_pay_cash_for_customer_reject_express)

    def start_pull_pre_pay_cash_for_customer_express(self, express_cost):
        ExpressService.start_pull_pre_pay_cash_for_customer_express(express_cost)

    start_pull_pre_pay_cash_for_customer_express = pyqtSlot(int)(start_pull_pre_pay_cash_for_customer_express)

    def start_pull_pre_pay_cash_for_customer_reject_express(self, express_cost):
        ExpressService.start_pull_pre_pay_cash_for_customer_reject_express(express_cost)

    start_pull_pre_pay_cash_for_customer_reject_express = pyqtSlot(int)(
        start_pull_pre_pay_cash_for_customer_reject_express)

    def start_free_mouth_by_size(self, mouth_size):
        BoxService.start_free_mouth_by_size(mouth_size)

    start_free_mouth_by_size = pyqtSlot(str)(start_free_mouth_by_size)

    # def stop_pay_cash_for_customer_express(self):
    #     ExpressService.stop_pay_cash_for_customer_express()
    #
    # stop_pay_cash_for_customer_express = pyqtSlot()(stop_pay_cash_for_customer_express)
    #
    # def stop_pay_cash_for_customer_reject_express(self):
    #     ExpressService.stop_pay_cash_for_customer_reject_express()
    #
    # stop_pay_cash_for_customer_reject_express = pyqtSlot()(stop_pay_cash_for_customer_reject_express)

    def start_get_customer_express_cost(self):
        ExpressService.start_get_customer_express_cost()

    start_get_customer_express_cost = pyqtSlot()(start_get_customer_express_cost)

    def start_get_customer_reject_express_cost(self):
        ExpressService.start_get_customer_reject_express_cost()

    start_get_customer_reject_express_cost = pyqtSlot()(start_get_customer_reject_express_cost)

    def start_store_customer_express(self):
        ExpressService.start_store_customer_express()

    start_store_customer_express = pyqtSlot()(start_store_customer_express)

    def start_store_customer_reject_express(self):
        ExpressService.start_store_customer_reject_express()

    start_store_customer_reject_express = pyqtSlot()(start_store_customer_reject_express)

    def start_customer_load_send_express_list(self, page):
        ExpressService.start_customer_load_send_express_list(page)

    start_customer_load_send_express_list = pyqtSlot(int)(start_customer_load_send_express_list)

    def start_customer_load_reject_express_list(self, page):
        ExpressService.start_customer_load_reject_express_list(page)

    start_customer_load_reject_express_list = pyqtSlot(int)(start_customer_load_reject_express_list)

    def start_load_customer_send_express_count(self):
        ExpressService.start_load_customer_send_express_count()

    start_load_customer_send_express_count = pyqtSlot()(start_load_customer_send_express_count)

    def start_load_customer_reject_express_count(self):
        ExpressService.start_load_customer_reject_express_count()

    start_load_customer_reject_express_count = pyqtSlot()(start_load_customer_reject_express_count)

    def start_courier_take_send_express(self, send_express_id_list):
        ExpressService.start_staff_take_send_express_list(send_express_id_list)

    start_courier_take_send_express = pyqtSlot(str)(start_courier_take_send_express)

    def start_courier_take_send_express_all(self):
        ExpressService.start_staff_take_all_send_express()

    start_courier_take_send_express_all = pyqtSlot()(start_courier_take_send_express_all)

    def start_courier_take_reject_express(self, send_express_id_list):
        ExpressService.start_staff_take_reject_express_list(send_express_id_list)

    start_courier_take_reject_express = pyqtSlot(str)(start_courier_take_reject_express)

    def start_courier_take_reject_express_all(self):
        ExpressService.start_staff_take_all_reject_express()

    start_courier_take_reject_express_all = pyqtSlot()(start_courier_take_reject_express_all)

    def start_load_mouth_list(self, page):
        BoxService.start_load_mouth_list(page)

    start_load_mouth_list = pyqtSlot(int)(start_load_mouth_list)

    def start_load_manager_mouth_count(self):
        BoxService.start_load_manager_mouth_count()

    start_load_manager_mouth_count = pyqtSlot()(start_load_manager_mouth_count)

    def start_manager_set_mouth(self, box_id, box_status):
        BoxService.start_manager_set_mouth(box_id, box_status)

    start_manager_set_mouth = pyqtSlot(str, str)(start_manager_set_mouth)

    def start_manager_open_mouth(self, mouth_id):
        BoxService.start_manager_open_mouth(mouth_id)

    start_manager_open_mouth = pyqtSlot(str)(start_manager_open_mouth)

    def start_manager_open_all_mouth(self):
        BoxService.start_manager_open_all_mouth()

    start_manager_open_all_mouth = pyqtSlot()(start_manager_open_all_mouth)

    def start_manager_set_free_mouth(self, box_id):
        BoxService.start_manager_set_free_mouth(box_id)

    start_manager_set_free_mouth = pyqtSlot(str)(start_manager_set_free_mouth)

    def courier_get_user(self):
        UserService.courier_get_user()

    courier_get_user = pyqtSlot()(courier_get_user)

    def start_choose_mouth_size(self, mouth_size, method_type):
        BoxService.start_choose_mouth_size(mouth_size, method_type)

    start_choose_mouth_size = pyqtSlot(str, str)(start_choose_mouth_size)

    def start_get_imported_express(self, text):
        ExpressService.start_get_imported_express(text)

    start_get_imported_express = pyqtSlot(str)(start_get_imported_express)

    def start_get_electronic_commerce_reject_express(self):
        ExpressService.start_get_electronic_commerce_reject_express()

    start_get_electronic_commerce_reject_express = pyqtSlot()(start_get_electronic_commerce_reject_express)

    def start_customer_reject_for_electronic_commerce(self, barcode):
        ExpressService.start_customer_reject_for_electronic_commerce(barcode)

    start_customer_reject_for_electronic_commerce = pyqtSlot(str)(start_customer_reject_for_electronic_commerce)

    def start_customer_reject_select_merchant(self, merchant_name):
        ExpressService.start_customer_reject_select_merchant(merchant_name)

    start_customer_reject_select_merchant = pyqtSlot(str)(start_customer_reject_select_merchant)

    def start_store_customer_reject_for_electronic_commerce(self):
        ExpressService.start_store_customer_reject_for_electronic_commerce()

    start_store_customer_reject_for_electronic_commerce = pyqtSlot()(
        start_store_customer_reject_for_electronic_commerce)

    def start_video_capture(self, filename):
        Camera.start_video_capture(filename)

    start_video_capture = pyqtSlot(str)(start_video_capture)

    def start_explorer(self):
        logging.warning('SYSTEM CLOSED BY OPERATOR')
        AlertService.system_closed_alert()
        os.system('start explorer.exe')

    start_explorer = pyqtSlot()(start_explorer)

    def start_get_product_file(self):
        ExpressService.start_get_product_file()

    start_get_product_file = pyqtSlot()(start_get_product_file)

    def start_get_card_info(self, text, identity):
        UserService.start_get_card_info(text, identity)

    start_get_card_info = pyqtSlot(str, str)(start_get_card_info)

    def start_open_main_box(self):
        BoxService.start_open_main_box()

    start_open_main_box = pyqtSlot()(start_open_main_box)

    # def start_download_source(self, a):
    #     DownloadService.start_download_source(a)
    #
    # start_download_source = pyqtSlot(str)(start_download_source)

    # def start_get_ad_file(self):
    #     AdvertisermentService.start_get_ad_file()
    #
    # start_get_ad_file = pyqtSlot()(start_get_ad_file)

    def start_get_express_info_by_barcode(self):
        ExpressService.start_get_express_info_by_barcode()

    start_get_express_info_by_barcode = pyqtSlot()(start_get_express_info_by_barcode)

    def stop_get_express_info_by_barcode(self):
        ExpressService.stop_get_express_info_by_barcode()

    stop_get_express_info_by_barcode = pyqtSlot()(stop_get_express_info_by_barcode)

    def start_get_express_info(self, express_text):
        PopboxService.start_get_express_info(express_text)

    start_get_express_info = pyqtSlot(str)(start_get_express_info)

    def start_get_popbox_express_info(self):
        PopboxService.start_get_popbox_express_info()

    start_get_popbox_express_info = pyqtSlot()(start_get_popbox_express_info)

    def start_get_customer_info_by_card(self):
        PopboxService.start_get_customer_info_by_card()

    start_get_customer_info_by_card = pyqtSlot()(start_get_customer_info_by_card)

    def start_payment_by_card(self):
        PopboxService.start_payment_by_card()

    start_payment_by_card = pyqtSlot()(start_payment_by_card)

    def start_get_cod_status(self):
        PopboxService.start_get_cod_status()

    start_get_cod_status = pyqtSlot()(start_get_cod_status)

    def start_reset_partial_transaction(self):
        PopboxService.start_reset_partial_transaction()

    start_reset_partial_transaction = pyqtSlot()(start_reset_partial_transaction)

    def start_post_capture_file(self):
        PopboxService.start_post_capture_file()

    start_post_capture_file = pyqtSlot()(start_post_capture_file)

    def start_get_capture_file_location(self):
        PopboxService.start_get_capture_file_location()

    start_get_capture_file_location = pyqtSlot()(start_get_capture_file_location)

    def start_get_locker_name(self):
        PopboxService.start_get_locker_name()

    start_get_locker_name = pyqtSlot()(start_get_locker_name)

    def start_global_payment_emoney(self, amount):
        PopboxService.start_global_payment_emoney(amount)

    start_global_payment_emoney = pyqtSlot(str)(start_global_payment_emoney)

    def start_get_sepulsa_product(self, phone):
        PopboxService.start_get_sepulsa_product(phone)

    start_get_sepulsa_product = pyqtSlot(str)(start_get_sepulsa_product)

    def start_sepulsa_transaction(self, phone_no, prod_id, prod_type, pay_type):
        PopboxService.start_sepulsa_transaction(phone_no, prod_id, prod_type, pay_type)

    start_sepulsa_transaction = pyqtSlot(str, str, str, str)(start_sepulsa_transaction)

    def start_push_data_settlement(self, type__):
        PopboxService.start_push_data_settlement(type__)

    start_push_data_settlement = pyqtSlot(str)(start_push_data_settlement)

    def start_get_gui_version(self):
        PopboxService.start_get_gui_version()

    start_get_gui_version = pyqtSlot()(start_get_gui_version)

    def get_popshop_product(self, category, maxcount):
        PopboxService.get_popshop_product(category, maxcount)

    get_popshop_product = pyqtSlot(str, str)(get_popshop_product)

    def get_query_member_popsend(self, phone):
        PopboxService.get_query_member_popsend(phone)

    get_query_member_popsend = pyqtSlot(str)(get_query_member_popsend)

    def start_popsend_topup(self, amount):
        PopboxService.start_popsend_topup(amount)

    start_popsend_topup = pyqtSlot(str)(start_popsend_topup)

    def get_locker_data(self, province, city):
        PopboxService.get_locker_data(province, city)

    get_locker_data = pyqtSlot(str, str)(get_locker_data)

    def start_popshop_transaction(self, product, customer, purchase, address):
        PopboxService.start_popshop_transaction(product, customer, purchase, address)

    start_popshop_transaction = pyqtSlot(str, str, str, str)(start_popshop_transaction)

    def get_popsend_button(self):
        PopboxService.get_popsend_button()

    get_popsend_button = pyqtSlot()(get_popsend_button)

    def sepulsa_trx_check(self, trx_id):
        PopboxService.sepulsa_trx_check(trx_id)

    sepulsa_trx_check = pyqtSlot(str)(sepulsa_trx_check)

    def start_deposit_express(self):
        PopboxService.start_deposit_express()

    start_deposit_express = pyqtSlot()(start_deposit_express)

    def box_start_migrate(self):
        PopboxService.box_start_migrate()

    box_start_migrate = pyqtSlot()(box_start_migrate)

    def set_tvc_player(self, command):
        set_tvc_player(command)

    set_tvc_player = pyqtSlot(str)(set_tvc_player)

    def start_get_ads_images(self, zpath):
        PopboxService.start_get_ads_images(zpath)

    start_get_ads_images = pyqtSlot(str)(start_get_ads_images)

    def start_delete_express_by_range(self, limit, duration):
        PopboxService.start_delete_express_by_range(limit, duration)

    start_delete_express_by_range = pyqtSlot(str, str)(start_delete_express_by_range)

    def start_post_gui_info(self):
        PopboxService.start_post_gui_info()

    start_post_gui_info = pyqtSlot()(start_post_gui_info)

    def start_post_subscribe_data(self, name, cust_data):
        PopboxService.start_post_subscribe_data(name, cust_data)

    start_post_subscribe_data = pyqtSlot(str, str)(start_post_subscribe_data)

    def start_user_bypass(self):
        BoxService.start_user_bypass()

    start_user_bypass = pyqtSlot()(start_user_bypass)

    def stop_user_bypass(self):
        BoxService.stop_user_bypass()

    stop_user_bypass = pyqtSlot()(stop_user_bypass)

    def start_get_file_dir(self, dir):
        PopboxService.start_get_file_dir(dir)

    start_get_file_dir = pyqtSlot(str)(start_get_file_dir)

    # def start_get_detection(self, method):
    #     PopboxService.start_get_detection(method)
    #
    # start_get_detection = pyqtSlot(str)(start_get_detection)

    def set_ext_keyboard(self, command):
        set_ext_keyboard(command)

    set_ext_keyboard = pyqtSlot(str)(set_ext_keyboard)

    def start_create_payment_yap(self, amount, cust_name, item, locker_name):
        PopboxService.start_create_payment_yap(amount, cust_name, item, locker_name)

    start_create_payment_yap = pyqtSlot(str, str, str, str)(start_create_payment_yap)

    def start_check_trans_global(self):
        PopboxService.start_check_trans_global()

    start_check_trans_global = pyqtSlot()(start_check_trans_global)

    def start_post_tvclog(self, media):
        PopboxService.start_post_tvclog(media)

    start_post_tvclog = pyqtSlot(str)(start_post_tvclog)

    def start_post_activity(self, activity):
        PopboxService.start_post_activity(activity)

    start_post_activity = pyqtSlot(str)(start_post_activity)

    def start_retry_login(self):
        UserService.start_retry_login()

    start_retry_login = pyqtSlot()(start_retry_login)

    def start_get_sim_product(self, operator, t_product):
        PPOB.start_get_sim_product(operator, t_product)

    start_get_sim_product = pyqtSlot(str, str)(start_get_sim_product)

    def start_check_addon(self, pid):
        PPOB.start_check_addon(pid)

    start_check_addon = pyqtSlot(str)(start_check_addon)

    def start_book_transaction(self, package):
        PPOB.start_book_transaction(package)

    start_book_transaction = pyqtSlot(str)(start_book_transaction)

    def start_confirm_transaction(self, invoice_id):
        PPOB.start_confirm_transaction(invoice_id)

    start_confirm_transaction = pyqtSlot(str)(start_confirm_transaction)

    def start_callback_transaction(self, package):
        PPOB.start_callback_transaction(package)

    start_callback_transaction = pyqtSlot(str)(start_callback_transaction)

    def start_create_transaction(self, type_of, package):
        PPOB.start_create_transaction(type_of, package)

    start_create_transaction = pyqtSlot(str, str)(start_create_transaction)

    def start_add_payment(self, package):
        PPOB.start_add_payment(package)

    start_add_payment = pyqtSlot(str)(start_add_payment)

    def start_get_payment(self):
        PPOB.start_get_payment()

    start_get_payment = pyqtSlot()(start_get_payment)

    def start_get_transaction(self, trx_id):
        PPOB.start_get_transaction(trx_id)

    start_get_transaction = pyqtSlot(str)(start_get_transaction)

    def start_flag_emoney(self, package):
        PPOB.start_flag_emoney(package)

    start_flag_emoney = pyqtSlot(str)(start_flag_emoney)

    def start_get_sepulsa(self, phone):
        PPOB.start_get_sepulsa(phone)

    start_get_sepulsa = pyqtSlot(str)(start_get_sepulsa)

    def start_get_ppob_button(self):
        PPOB.start_get_ppob_button()

    start_get_ppob_button = pyqtSlot()(start_get_ppob_button)

    def start_filter_sim_data(self, filtering):
        PPOB.start_filter_sim_data(filtering)

    start_filter_sim_data = pyqtSlot(str)(start_filter_sim_data)

    def start_store_apexpress(self, phone, hour):
        APService.start_store_apexpress(phone, hour)

    start_store_apexpress = pyqtSlot(str, str)(start_store_apexpress)

    def start_store_ap_transaction(self, param):
        APService.start_store_ap_transaction(param)

    start_store_ap_transaction = pyqtSlot(str)(start_store_ap_transaction)

    def start_update_apexpress(self, validateCode):
        APService.start_update_apexpress(validateCode)

    start_update_apexpress = pyqtSlot(str)(start_update_apexpress)

    def start_idle_mode(self):
        PullMessage.start_idle_mode()

    start_idle_mode = pyqtSlot()(start_idle_mode)

    def stop_idle_mode(self):
        PullMessage.stop_idle_mode()

    stop_idle_mode = pyqtSlot()(stop_idle_mode)

    def start_create_payment_tcash(self, amount, cust_name, item, locker_name):
        PopboxService.start_create_payment_tcash(amount, cust_name, item, locker_name)

    start_create_payment_tcash = pyqtSlot(str, str, str, str)(start_create_payment_tcash)

    def start_create_payment_ottoqr(self, amount, cust_name, item, locker_name):
        PopboxService.start_create_payment_ottoqr(amount, cust_name, item, locker_name)

    start_create_payment_ottoqr = pyqtSlot(str, str, str, str)(start_create_payment_ottoqr)

    def start_create_payment_gopay(self, amount, cust_name, item, locker_name):
        PopboxService.start_create_payment_gopay(amount, cust_name, item, locker_name)

    start_create_payment_gopay = pyqtSlot(str, str, str, str)(start_create_payment_gopay)

    def store_direct_popsafe(self, param):
        PopboxService.store_direct_popsafe(param)

    store_direct_popsafe = pyqtSlot(str)(store_direct_popsafe)

    def start_check_member(self, phone):
        PPOB.start_check_member(phone)
    
    start_check_member = pyqtSlot(str)(start_check_member)

    def start_extend_express(self, express_no):
        PopboxService.start_extend_express(express_no)

    start_extend_express = pyqtSlot(str)(start_extend_express) 


def signal_handler():
    ExpressService._EXPRESS_.product_file_signal.connect(view.rootObject().product_file_result)
    ExpressService._EXPRESS_.customer_take_express_signal.connect(
        view.rootObject().customer_take_express_result)
    BoxService._BOX_.mouth_number_signal.connect(view.rootObject().mouth_number_result)
    BoxService._BOX_.manager_get_box_info_signal.connect(view.rootObject().manager_get_box_info_result)
    BoxService._BOX_.get_version_signal.connect(view.rootObject().get_version_result)
    UserService._USER_.user_login_signal.connect(view.rootObject().user_login_result)
    ExpressService._EXPRESS_.barcode_signal.connect(view.rootObject().barcode_result)
    ExpressService._EXPRESS_.store_express_signal.connect(view.rootObject().store_express_result)
    ExpressService._EXPRESS_.phone_number_signal.connect(view.rootObject().phone_number_result)
    ExpressService._EXPRESS_.paid_amount_signal.connect(view.rootObject().paid_amount_result)
    UserService._USER_.user_info_signal.connect(view.rootObject().user_info_result)
    ExpressService._EXPRESS_.customer_store_express_signal.connect(
        view.rootObject().customer_store_express_result)
    BoxService._BOX_.mouth_status_signal.connect(view.rootObject().mouth_status_result)
    BoxService._BOX_.free_mouth_num_signal.connect(view.rootObject().free_mouth_result)
    ExpressService._EXPRESS_.overdue_express_list_signal.connect(
        view.rootObject().overdue_express_list_result)
    ExpressService._EXPRESS_.overdue_express_count_signal.connect(
        view.rootObject().overdue_express_count_result)
    BoxService._BOX_.init_client_signal.connect(view.rootObject().init_client_result)
    ExpressService._EXPRESS_.staff_take_overdue_express_signal.connect(
        view.rootObject().courier_take_overdue_express_result)
    ExpressService._EXPRESS_.load_express_list_signal.connect(view.rootObject().load_express_list_result)
    ExpressService._EXPRESS_.customer_store_express_cost_signal.connect(
        view.rootObject().customer_store_express_cost_result)
    ExpressService._EXPRESS_.customer_express_cost_insert_coin_signal.connect(
        view.rootObject().customer_express_cost_insert_coin_result)
    ExpressService._EXPRESS_.store_customer_express_result_signal.connect(
        view.rootObject().store_customer_express_result_result)
    ExpressService._EXPRESS_.send_express_list_signal.connect(view.rootObject().send_express_list_result)
    ExpressService._EXPRESS_.send_express_count_signal.connect(view.rootObject().send_express_count_result)
    ExpressService._EXPRESS_.staff_take_send_express_signal.connect(
        view.rootObject().take_send_express_result)
    BoxService._BOX_.manager_mouth_count_signal.connect(view.rootObject().manager_mouth_count_result)
    BoxService._BOX_.load_mouth_list_signal.connect(view.rootObject().load_mouth_list_result)
    BoxService._BOX_.mouth_list_signal.connect(view.rootObject().mouth_list_result)
    BoxService._BOX_.manager_set_mouth_signal.connect(view.rootObject().manager_set_mouth_result)
    BoxService._BOX_.manager_open_mouth_by_id_signal.connect(
        view.rootObject().manager_open_mouth_by_id_result)
    UserService._USER_.courier_get_user_signal.connect(view.rootObject().courier_get_user_result)
    BoxService._BOX_.manager_open_all_mouth_signal.connect(view.rootObject().manager_open_all_mouth_result)
    BoxService._BOX_.choose_mouth_signal.connect(view.rootObject().choose_mouth_result)
    BoxService._BOX_.free_mouth_by_size_result.connect(view.rootObject().free_mouth_by_size_result)
    ExpressService._EXPRESS_.imported_express_result_signal.connect(
        view.rootObject().imported_express_result)
    ExpressService._EXPRESS_.customer_reject_express_signal.connect(
        view.rootObject().customer_reject_express_result)
    ExpressService._EXPRESS_.reject_express_signal.connect(view.rootObject().reject_express_result)
    ExpressService._EXPRESS_.reject_express_list_signal.connect(
        view.rootObject().reject_express_list_result)
    BoxService._BOX_.open_main_box_signal.connect(view.rootObject().open_main_box_result)
    PopboxService._POP_.start_get_express_info_result_signal.connect(
        view.rootObject().start_get_express_info_result)
    PopboxService._POP_.start_get_popbox_express_info_signal.connect(
        view.rootObject().start_get_popbox_express_info_result)
    PopboxService._POP_.start_get_customer_info_by_card_signal.connect(
        view.rootObject().start_get_customer_info_by_card_result)
    PopboxService._POP_.start_payment_by_card_signal.connect(
        view.rootObject().start_payment_by_card_result)
    PopboxService._POP_.start_get_cod_status_signal.connect(view.rootObject().start_get_cod_status_result)
    PopboxService._POP_.start_reset_partial_transaction_signal.connect(
        view.rootObject().start_reset_partial_transaction_result)
    PopboxService._POP_.start_post_capture_file_signal.connect(
        view.rootObject().start_post_capture_file_result)
    PopboxService._POP_.start_get_locker_name_signal.connect(
        view.rootObject().start_get_locker_name_result)
    PopboxService._POP_.start_global_payment_emoney_signal.connect(
        view.rootObject().start_global_payment_emoney_result)
    PopboxService._POP_.start_get_sepulsa_product_signal.connect(
        view.rootObject().start_get_sepulsa_product_result)
    PopboxService._POP_.start_sepulsa_transaction_signal.connect(
        view.rootObject().start_sepulsa_transaction_result)
    PopboxService._POP_.start_get_gui_version_signal.connect(
        view.rootObject().start_get_gui_version_result)
    PopboxService._POP_.start_customer_scan_qr_code_signal.connect(
        view.rootObject().start_customer_scan_qr_code_result)
    PopboxService._POP_.stop_customer_scan_qr_code_signal.connect(
        view.rootObject().stop_customer_scan_qr_code_result)
    PopboxService._POP_.get_popshop_product_signal.connect(view.rootObject().get_popshop_product_result)
    PopboxService._POP_.get_query_member_popsend_signal.connect(
        view.rootObject().get_query_member_popsend_result)
    PopboxService._POP_.start_popsend_topup_signal.connect(view.rootObject().start_popsend_topup_result)
    PopboxService._POP_.get_locker_data_signal.connect(view.rootObject().get_locker_data_result)
    PopboxService._POP_.start_popshop_transaction_signal.connect(
        view.rootObject().start_popshop_transaction_result)
    PopboxService._POP_.get_popsend_button_signal.connect(view.rootObject().get_popsend_button_result)
    PopboxService._POP_.sepulsa_trx_check_signal.connect(view.rootObject().sepulsa_trx_check_result)
    PopboxService._POP_.box_start_migrate_signal.connect(view.rootObject().box_start_migrate_result)
    PopboxService._POP_.start_get_ads_images_signal.connect(view.rootObject().get_ads_images_result)
    PopboxService._POP_.start_post_subscribe_data_signal.connect(
        view.rootObject().start_post_subscribe_data_result)
    PopboxService._POP_.start_get_file_dir_signal.connect(view.rootObject().start_get_file_dir_result)
    PopboxService._POP_.start_get_detection_signal.connect(view.rootObject().start_get_detection_result)
    PopboxService._POP_.start_create_payment_signal.connect(view.rootObject().start_create_payment_result)
    PopboxService._POP_.start_check_trans_global_signal.connect(
        view.rootObject().check_trans_global_result)
    PPOB._PPOB_.start_get_sim_product_signal.connect(view.rootObject().get_sim_card_result)
    PPOB._PPOB_.start_check_addon_signal.connect(view.rootObject().check_addon_result)
    PPOB._PPOB_.start_book_transaction_signal.connect(view.rootObject().book_transaction_result)
    PPOB._PPOB_.start_confirm_transaction_signal.connect(
        view.rootObject().confirm_transaction_result)
    PPOB._PPOB_.start_callback_signal.connect(view.rootObject().callback_result)
    PPOB._PPOB_.start_create_transaction_signal.connect(view.rootObject().create_transaction_result)
    PPOB._PPOB_.start_add_payment_signal.connect(view.rootObject().add_payment_result)
    PPOB._PPOB_.start_get_payment_signal.connect(view.rootObject().get_payment_result)
    PPOB._PPOB_.start_get_transaction_signal.connect(view.rootObject().get_transaction_result)
    PPOB._PPOB_.start_flag_emoney_signal.connect(view.rootObject().flag_emoney_result)
    PPOB._PPOB_.start_get_ppob_button_signal.connect(view.rootObject().ppob_button_result)
    APService._AP_.start_store_apexpress_signal.connect(view.rootObject().apexpress_result)
    APService._AP_.start_update_apexpress_signal.connect(view.rootObject().update_apexpress_result)
    PPOB._PPOB_.start_check_member_signal.connect(view.rootObject().start_check_member_result)


_LOG_ = None


def configuration_log():
    global _LOG_
    try:
        if not os.path.exists(sys.path[0] + '/log/'):
            os.makedirs(sys.path[0] + '/log/')
        handler = logging.handlers.TimedRotatingFileHandler(filename=sys.path[0] + '/log/base.log',
                                                            when='MIDNIGHT',
                                                            interval=1,
                                                            backupCount=60)
        logging.basicConfig(handlers=[handler],
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(funcName)s:%(lineno)d: %(message)s',
                            datefmt='%d/%m %H:%M:%S')
        _LOG_ = logging.getLogger()
    except Exception as e:
        print("Logging Configuration ERROR : ", e)


def get_disk_info():
    encrypt_str = ''
    disk_info = []
    try:
        c = wmi.WMI()
        for physical_disk in c.Win32_DiskDrive():
            encrypt_str = encrypt_str + physical_disk.SerialNumber.strip()
    except Exception as e:
        encrypt_str = 'None'
        _LOG_.warning(('Error Getting Disk Info : ', e))

    disk_info.append(encrypt_str)
    HttpClient._DISK_SN_ = disk_info[0]


def get_screen_resolution():
    try:
        import ctypes
        user32 = ctypes.windll.user32
        # user32.SetProcessDPIAware()
        res = [user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)]
        _LOG_.info(('Detected Screen Resolution : ', str(res)))
    except Exception as e:
        res = [0, 0]
        _LOG_.warning(('Error get screen resolution : ', e))
    # print(res)
    return res


def process_exists(processname):
    tlcall = 'TASKLIST', '/FI', 'imagename eq %s' % processname
    tlproc = subprocess.Popen(tlcall, shell=True, stdout=subprocess.PIPE)
    tlout = tlproc.communicate()[0].decode('utf-8').strip().split("\r\n")
    # print('Checking Process for : ' + processname)
    if len(tlout) > 1 and processname in tlout[-1]:
        print(processname + ' is Found!')
        return True
    else:
        # print(processname + ' is not Found!')
        return False


def set_tvc_player(command):
    global GLOBAL_SETTING
    if command == "":
        return
    elif command == "STOP":
        os.system(sys.path[0] + '/player/stop.bat')
    elif command == "START":
        if process_exists("PopBoxTVCPlayer.scr"):
            os.system(sys.path[0] + '/player/stop.bat')
        os.system(sys.path[0] + '/player/start.bat')
    else:
        return


def check_database(data_name):
    if not os.path.exists(sys.path[0] + '/database/' + data_name + '.db'):
        ClientDatabase.init_database()
    _LOG_.info(("DB : ", data_name))


def init_reader():
    try:
        init__emoney = device.QP3000S.init_serial(x=1)
        _LOG_.info(('init_emoney flag is ', init__emoney))
        if init__emoney:
            init_result = device.QP3000S.initSAM()
            sync_time = device.QP3000S.syncTime()
            Configurator.set_value('panel', 'emoney', 'enabled')
            _LOG_.info(('init_SAM & sync_Time result is ', init_result, sync_time))
        else:
            Configurator.set_value('panel', 'emoney', 'disabled')
            _LOG_.warning('init_SAM & sync_Time result is ERROR')
    except Exception as e:
        Configurator.set_value('panel', 'emoney', 'disabled')
        _LOG_.warning(str(e))


def kill_explorer():
    if GLOBAL_SETTING['dev^mode'] is False:
        os.system('taskkill /f /im explorer.exe')
    else:
        _LOG_.info('Development Mode is ON')


def disable_screensaver():
    try:
        os.system('reg delete "HKEY_CURRENT_USER\Control Panel\Desktop" /v SCRNSAVE.EXE /f')
    except Exception as e:
        _LOG_.warning(('Screensaver Disabling ERROR : ', e))


def init_nfc_reader():
    if GLOBAL_SETTING['reader'] and not GLOBAL_SETTING['dev^mode']:
        init_reader()
    else:
        _LOG_.info('eMoney Reader is not ACTIVATED!')


def init_time(file):
    if file is None:
        return
    try:
        import platform
        os_ver = platform.platform()
        if 'Windows-7' in os_ver:
            process = subprocess.Popen(sys.path[0] + file, shell=True, stdout=subprocess.PIPE)
            output = process.communicate()[0].decode('utf-8').strip().split("\r\n")
            _LOG_.info(('time initiation is success : ', str(output)))
            sleep(5)
        else:
            _LOG_.debug(('time initiation is failed : ', str(os_ver)))
    except Exception as e:
        _LOG_.warning(('time initiation is failed : ', e))
    # print(('init time result : ', init_time_result))


def check_path(new):
    try:
        process = subprocess.Popen("PATH", shell=True, stdout=subprocess.PIPE)
        output = process.communicate()[0].decode('utf-8').strip().split("\r\n")
        output_ = output[0].split(";")
        if new in output_:
            return True
        else:
            return False
    except Exception as e:
        _LOG_.warning(('check_path is failed : ', e))
        return False


def set_ext_keyboard(command):
    if command == "":
        return
    elif command == "STOP":
        os.system('taskkill /f /IM osk.exe')
    elif command == "START":
        if not process_exists('osk.exe'):
            os.system('osk')
        else:
            print('External Keyboard is already running..!')
    else:
        return


UPDATE_SCRIPT = 'update.script.txt'
COUNTRY_SET = 'INA'
APP_PATH = ''


def generate_script(c, m, d=None):
    global APP_PATH
    _ = '''option batch abort
option confirm off
open sftp://root:4L*p{9vL}cqER(=G@pr0x-sync.popbox.asia/ -hostkey="ssh-ed25519 256 b5:e9:e2:9a:22:c6:cc:dc:a7:35:c9:89:b4:51:e7:fc" -timeout=600 
synchronize local [::] /home/gui/]::[
close
exit
'''
    r = 'gui' if m is False else 'dev'
    f = {
        'local': 'popbox_gui_ina' if c == 'INA' else 'popbox_gui_mly',
        'remote': 'popbox_'+r+'_ina' if c == 'INA' else 'popbox_'+r+'_mly'
    }
    APP_PATH = f['local']
    s = _.replace('[::]', f['local']).replace(']::[', f['remote'])
    if d is not None:
        s = _.replace('[::]', d).replace(']::[', f['remote'])
    # if not os.path.exists(os.path.join('D:\\', f['local'])):
    #     move_app(f['local'])
    #     sys.exit(0)
    with open(os.path.join(sys.path[0], UPDATE_SCRIPT), 'w+') as w:
        w.write(s)
    return s


def start_update_app(country):
    global GLOBAL_SETTING, COUNTRY_SET
    COUNTRY_SET = country
    GLOBAL_SETTING['update^script'] = generate_script(c=country, m=GLOBAL_SETTING['dev^mode'], d=os.getcwd())
    os.system('@echo off && WinSCP.com /script='+UPDATE_SCRIPT)
    print('Initiated Setting : ', GLOBAL_SETTING)


def move_app(a):
    n = os.path.join('D:\\', a)
    try:
        os.system('xcopy /hiery '+os.getcwd()+' '+n)
        # print('execute command : ', 'xcopy '+os.getcwd()+' '+n)
    except Exception as e:
        print('execute command on move_app : ', str(e))


# def restart_app():
#     if COUNTRY_SET == 'INA':
#         os.system(os.path.join('D:\\', 'popbox_gui_ina', 'guard.bat'))
#     else:
#         os.system(os.path.join('D:\\', 'popbox_gui_mly', 'guard.bat'))


@atexit.register
def exit_message():
    print('[Info] Please re-run the app from : ', os.path.join('D:\\', APP_PATH))


GLOBAL_SETTING = dict()


def init_setting():
    global GLOBAL_SETTING
    # Hard-Coded Setting
    PopboxService.global_token = Configurator.set_value('popbox', 'poptoken', '5701d8ef69b37f1a3210eed1df3b5ba0f6b0a30a')
    PopboxService.global_url = Configurator.set_value('popbox', 'popserver', 'http://lockerapi.popbox.asia/')
    # TODO URGENT
    Configurator.set_value('popbox', 'dummy^transaction', '0')
    Configurator.set_value('panel', 'force^test', '0')
    # Configurator.set_value('panel', 'ssl^verify', '1')
    # TODO URGENT
    init_reader()

    if Configurator.get_value('panel', 'gui') == "development":
        GLOBAL_SETTING['dev^mode'] = True
    else:
        GLOBAL_SETTING['dev^mode'] = False
    if Configurator.get_value('panel', 'emoney') == "enabled":
        GLOBAL_SETTING['reader'] = True
    else:
        GLOBAL_SETTING['reader'] = False
    if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
        GLOBAL_SETTING['db'] = 'pakpobox'
    else:
        GLOBAL_SETTING['db'] = 'popboxclient'
    GLOBAL_SETTING['display'] = get_screen_resolution()
    GLOBAL_SETTING['use^mode'] = Configurator.get_or_set_value('ClientInfo', 'use^mode', 'regular')
    GLOBAL_SETTING['backend'] = Configurator.get_value('ClientInfo', 'serveraddress')
    GLOBAL_SETTING['language'] = 'first.qm'
    if GLOBAL_SETTING['use^mode'] == 'apservice':
        if GLOBAL_SETTING['display'] == [1080, 1920]:
            GLOBAL_SETTING['main^module'] = 'MainAP32.qml'
        else:
            GLOBAL_SETTING['main^module'] = 'MainAP.qml'
    elif GLOBAL_SETTING['use^mode'] == 'regular':
        if GLOBAL_SETTING['display'] == [1080, 1920]:
            GLOBAL_SETTING['main^module'] = 'Main32.qml'
        else:
            GLOBAL_SETTING['main^module'] = 'Main.qml'
    # setting['detector'] = Detector.setting
    _LOG_.info(GLOBAL_SETTING)
    return GLOBAL_SETTING


def update_module(module_list):
    if len(module_list) == 0 or module_list is None:
        return
    try:
        if check_path("C:\Python34\Scripts") is False:
            os.system("PATH %PATH%;C:\Python34\Scripts")
        for mod in module_list:
            subprocess.call("pip install --upgrade " + mod)
            _LOG_.info((str(mod), 'is updated successfully'))
    except Exception as e:
        _LOG_.warning(("update module is failed : ", e))


def update_tvc_activity_32():
    tvc_history = []
    # log_file = os.path.join(os.getcwd(), 'player', 'last_history.log')
    print('TVC Update Status... [STARTED]')
    attempt = 0
    while True:
        all_tvc = os.listdir(os.path.join(os.getcwd(), 'advertisement', 'video32'))
        if attempt == 0 or attempt % 60 == 0:
            PopboxService.post_tvclist(json.dumps(all_tvc))
        if len(tvc_history) == len(all_tvc):
            tvc_history.clear()
        attempt += 1
        try:
            if process_exists('PopBoxTVCPlayer.scr'):
                current_tvc = get_current_tvc_32()
                if current_tvc is not None and current_tvc not in tvc_history:
                    PopboxService.post_tvclog(current_tvc)
                    tvc_history.append(current_tvc)
                    print('TVC Update Status... [' + current_tvc + ']')
        except Exception as e:
            print('TVC Update Status... [STOP]')
            _LOG_.warning(str(e))
        sleep(5)


def sync_tvc_32():
    ClientTools.get_global_pool().apply_async(update_tvc_activity_32)


REG_PATH = r'SOFTWARE\PopBox\PopBoxTVCPlayer'
PLAY_AUDIO = 'True'
MOVIE_PATH = os.getcwd() + r'\advertisement\video32'


def set_reg_tvc_player32():
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, REG_PATH)
        reg_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0, winreg.KEY_WRITE)
        winreg.SetValueEx(reg_key, 'PlayAudio', 0, winreg.REG_SZ, PLAY_AUDIO)
        winreg.SetValueEx(reg_key, 'MoviePath', 0, winreg.REG_SZ, MOVIE_PATH)
        winreg.CloseKey(reg_key)
        print('Registering TVC PLayer 32 Key... [SUCCESS]')
    except WindowsError:
        print('Registering TVC PLayer 32 Key... [FAILED]')


def get_current_tvc_32():
    try:
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0, winreg.KEY_READ)
        value, regtype = winreg.QueryValueEx(registry_key, 'LastMovie')
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None


if __name__ == '__main__':
    # ClientTools.reset_system_time_date()
    configuration_log()
    init_time('/neutronSync.bat')
    update_module({})
    init_setting()
    kill_explorer()     # TODO URGENT
    start_update_app('INA')     # TODO URGENT
    check_database(GLOBAL_SETTING['db'])
    path = sys.path[0] + '/qml/'
    if os.name == 'nt':
        path = 'qml/'
    slot_handler = SlotHandler()
    app = QGuiApplication(sys.argv)
    view = QQuickView()
    context = view.rootContext()
    translator = QTranslator()
    context.setContextProperty('slot_handler', slot_handler)
    translator.load(path + GLOBAL_SETTING['language'])
    app.installTranslator(translator)
    view.engine().quit.connect(app.quit)
    view.setSource(QUrl(path + GLOBAL_SETTING['main^module']))
    signal_handler()
    if not GLOBAL_SETTING['dev^mode']:
        app.setOverrideCursor(Qt.BlankCursor)     # TODO URGENT
        # pass
    view.setFlags(Qt.WindowFullscreenButtonHint)
    view.setFlags(Qt.FramelessWindowHint)
    if GLOBAL_SETTING['display'] == [1080, 1920]:
        view.resize(1079, 1920)
        set_reg_tvc_player32()
    else:
        view.resize(1023, 768)
    disable_screensaver()
    get_disk_info()
    view.show()
    PopboxService.start_delete_express_by_range(limit=1, duration='MONTH')
    PopboxService.start_post_gui_info()
    PushMessage.start_sync_message()
    PullMessage.start_pull_message()
    if GLOBAL_SETTING['display'] == [1080, 1920]:
        sync_tvc_32()
    pygame.init()
    Camera.init_camera()
    app.exec_()
    del view
